package ch.genevashop.software.torrent.benconding;

public class BReadException extends Exception {
	
	public BReadException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 7873535655961703488L;

}
