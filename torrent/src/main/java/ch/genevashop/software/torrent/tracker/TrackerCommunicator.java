package ch.genevashop.software.torrent.tracker;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ch.genevashop.software.torrent.Properties;
import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.peer.Peer;

public abstract class TrackerCommunicator {

	private long uploaded = 0;
	private long downloaded = 0;
	private long left = 0;
	private int complete = 0;
	private int incomplete = 0;
	private int intervalInSecond = 0;
	private int minIntervalInSecond = 0;
	private byte[] peers;
	private final Map<String, Peer> peerslist = new ConcurrentHashMap<>();
	private Torrent torrent;
	private Properties properties;
	private Status status;

	{
		status = Status.WAITING;
	}

	public TrackerCommunicator(Torrent torrent, Properties properties) {
		this.torrent = torrent;
		this.properties = properties;
	}

	public abstract boolean connect(URI uri, int left);

	public long getUploaded() {
		return uploaded;
	}

	public void setUploaded(long uploaded) {
		this.uploaded = uploaded;
	}

	public long getDownloaded() {
		return downloaded;
	}

	public void setDownloaded(long downloaded) {
		this.downloaded = downloaded;
	}

	public long getLeft() {
		return left;
	}

	public void setLeft(long left) {
		this.left = left;
	}

	public void setLeft(int left) {
		this.left = left;
	}

	public int getComplete() {
		return complete;
	}

	public void setComplete(int complete) {
		this.complete = complete;
	}

	public int getIncomplete() {
		return incomplete;
	}

	public void setIncomplete(int incomplete) {
		this.incomplete = incomplete;
	}

	public int getIntervalInSecond() {
		return intervalInSecond;
	}

	public void setIntervalInSecond(int intervalInSecond) {
		this.intervalInSecond = intervalInSecond;
	}

	public int getMinIntervalInSecond() {
		return minIntervalInSecond;
	}

	public void setMinIntervalInSecond(int minIntervalInSecond) {
		this.minIntervalInSecond = minIntervalInSecond;
	}

	public byte[] getPeers() {
		return peers;
	}

	public void setPeers(byte[] peers) {
		this.peers = peers;
	}

	public Map<String, Peer> getPeerslist() {
		return peerslist;
	}

	public Torrent getTorrent() {
		return torrent;
	}

	public void setTorrent(Torrent torrent) {
		this.torrent = torrent;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
