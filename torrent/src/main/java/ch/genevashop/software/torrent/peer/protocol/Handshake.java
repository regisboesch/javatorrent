package ch.genevashop.software.torrent.peer.protocol;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

public class Handshake {

	private static final String TORRENT_ENCODING = "ISO-8859-1";
	public static final int SIZEHANDSHAKE = 68;
	private static final String PROTOCOLVERSION = "BitTorrent protocol";

	public static byte[] validateHandshakeAndGeetPeerID(ByteBuffer buffer) {
		byte peer_id[] = null;

		int pstrlen = Byte.valueOf(buffer.get()).intValue();

		if (pstrlen < 0) {
			return null;
		}

		byte[] pstr = new byte[pstrlen];
		buffer.get(pstr);

		try {
			if (!PROTOCOLVERSION.equals(new String(pstr, TORRENT_ENCODING))) {
				return null;
			}
		} catch (UnsupportedEncodingException e) {
			return null;
		}

		// Ignore reserved bytes
		byte[] reserved = new byte[8];
		buffer.get(reserved);
		
		byte[] infoHash = new byte[20];
		buffer.get(infoHash);
		
		peer_id = new byte[20];
		buffer.get(peer_id);
		
		return peer_id;
	}

	public static ByteBuffer createHandshake(byte[] info_hash, String peer_id) {

		ByteBuffer bb = ByteBuffer.allocate(SIZEHANDSHAKE);
		bb.clear();

		// pstrlen
		bb.put((byte) 0x13);

		// protocol
		try {
			bb.put(PROTOCOLVERSION.getBytes(TORRENT_ENCODING));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// Reserved
		bb.putLong(new Long(0));

		// Info_hash
		bb.put(info_hash);

		// Peer_id
		bb.put(peer_id.getBytes());

		return bb;
	}
}
