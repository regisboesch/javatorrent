package ch.genevashop.software.torrent.policy;

import java.util.Map;

import ch.genevashop.software.torrent.file.TorrentFile;

public interface PiecePolicy {
	public Integer getNextPiece(Map<Integer, Integer> map, TorrentFile torrentFile);
	public void setPieceComplete(int piece);
}
