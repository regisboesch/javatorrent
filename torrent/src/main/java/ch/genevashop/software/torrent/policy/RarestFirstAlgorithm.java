package ch.genevashop.software.torrent.policy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.genevashop.software.tools.MapTools;
import ch.genevashop.software.torrent.file.TorrentFile;

public class RarestFirstAlgorithm implements PiecePolicy {

	static final Logger log = Logger.getLogger(RarestFirstAlgorithm.class);
	private Integer currentPiece;

	{
		currentPiece = null;
	}

	@Override
	public Integer getNextPiece(Map<Integer, Integer> map,
			TorrentFile torrentFile) {

		if (currentPiece == null) {

			if (torrentFile.hasNoPieceDownloaded()) {
				// Return random piece
				List<Integer> keysList = new ArrayList<Integer>(map.keySet());
				Collections.shuffle(keysList);
				
				if (!keysList.isEmpty()) {
					currentPiece = keysList.get(0);
				}
			} else if (!torrentFile.isAlmostDone()) {
				// Sort Map by Value
				Map<Integer, Integer> sortedMap = new LinkedHashMap<>(
						map.size());
				sortedMap = MapTools.sortByValue(map);
				
				// Get the first one (rarest)
				for (Integer entry : sortedMap.keySet()) {
					currentPiece = entry;
					return currentPiece;
				}
				currentPiece = null;
			}
		} 		
		return currentPiece;
	}

	@Override
	public void setPieceComplete(int piece) {
		if (piece==currentPiece) {
			currentPiece= null;
		}
	}

}
