package ch.genevashop.software.torrent.peer;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicInteger;

import ch.genevashop.software.torrent.peer.metrics.DataMetrics;
import ch.genevashop.software.torrent.peer.metrics.PeerMetrics;

public class Peer implements Serializable {

	private static final long serialVersionUID = -7179814981670249114L;
	private PeerState internalState;
	private InetSocketAddress address;
	private byte[] peer_id;
	private final PeerMetrics peerMetrics = new PeerMetrics();
	private boolean iamchoked = true;
	private boolean iaminterested = false;
	private boolean peerchoked = true;
	private boolean peerinterested = false;
	private final AtomicInteger numberOfChunkAssigned = new AtomicInteger(0);
	
	public Peer(InetSocketAddress address) {
		super();
		this.address = address;
		this.setInternalState(PeerState.NOTCONNECTED);
	}

	public InetSocketAddress getAddress() {
		return address;
	}

	public void setAddress(InetSocketAddress address) {
		this.address = address;
	}

	public PeerState getInternalState() {
		return internalState;
	}

	public void setInternalState(PeerState internalState) {
		this.internalState = internalState;
	}

	@Override
	public String toString() {
		return address.toString();
	}

	public byte[] getPeer_id() {
		return peer_id;
	}

	public void setPeer_id(byte[] peer_id) {
		this.peer_id = peer_id;
	}

	public PeerMetrics getPeerMetrics() {
		return peerMetrics;
	}

	public void updateUpload(int size) {
		peerMetrics.updateUpload(size);
	}

	public void updateDownload(int size) {
		peerMetrics.updateDownload(size);
	}

	public DataMetrics getMetrics() {
		peerMetrics.computeRatio();
		return new DataMetrics(peerMetrics.getLastUploadRatio(),
				peerMetrics.getLastDowndloadRatio());
	}

	public boolean isIamchoked() {
		return iamchoked;
	}

	public void setIamchoked(boolean iamchoked) {
		this.iamchoked = iamchoked;
	}

	public boolean isIaminterested() {
		return iaminterested;
	}

	public void setIaminterested(boolean iaminterested) {
		this.iaminterested = iaminterested;
	}

	public boolean isPeerchoked() {
		return peerchoked;
	}

	public void setPeerchoked(boolean peerchoked) {
		this.peerchoked = peerchoked;
	}

	public boolean isPeerinterested() {
		return peerinterested;
	}

	public void setPeerinterested(boolean peerinterested) {
		this.peerinterested = peerinterested;
	}	
		
	public void increaseChunkByOne() {
		numberOfChunkAssigned.incrementAndGet();
	}
	
	public void decreaseChunkByOne() {
		numberOfChunkAssigned.decrementAndGet();
	}
	
	public int getCurrentChunkAssigned() {
		return numberOfChunkAssigned.get();
	}
}
