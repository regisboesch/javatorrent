package ch.genevashop.software.torrent.peer;

import java.util.Date;

public class Chunk {

	private long lastTime;
	private int piece;
	private int offset;
	private int length;
	private boolean done;
	private byte data[];
	private String currentPeers;
	
	public Chunk(int piece, int offset, int length) {
		super();
		this.piece = piece;
		this.offset = offset;
		this.length = length;
		this.lastTime = new Date().getTime() - 30 * 1000;
		done = false;
	}

	public long getLastTime() {
		return lastTime;
	}

	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}

	public int getPiece() {
		return piece;
	}

	public void setPiece(int piece) {
		this.piece = piece;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public String toString() {
		return "Chunk piece[" + piece + "], offset [" + offset + "], length ["
				+ length + "]" + "peer ["+ currentPeers +"]";
	}

	public String getCurrentPeers() {
		return currentPeers;
	}

	public void setCurrentPeers(String currentPeers) {
		this.currentPeers = currentPeers;
	}
	
	
}
