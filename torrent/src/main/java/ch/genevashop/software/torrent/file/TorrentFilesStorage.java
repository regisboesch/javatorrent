package ch.genevashop.software.torrent.file;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.nio.file.StandardOpenOption.READ;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;

import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.metainfo.TorrentFiles;
import ch.genevashop.software.torrent.tracker.TorrentTracker;

public class TorrentFilesStorage implements TorrentStorage {

	static final Logger log = Logger.getLogger(TorrentTracker.class);
	private Torrent torrent;


	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.file.TorrentStorage#initializeTorrentFiles()
	 */
	@Override
	public void initializeTorrentFiles(Torrent torrent) {
		this.torrent = torrent;
		
		for (TorrentFiles files : torrent.getTorrentFiles()) {

			Path p = this.checkAndCreatePath(torrent.getRootName(), files.getName());

			if (!Files
					.exists(p, new LinkOption[] { LinkOption.NOFOLLOW_LINKS })) {
				ByteBuffer bb = ByteBuffer.allocate(files.getLength());
				try (SeekableByteChannel sbc = Files.newByteChannel(p,
						EnumSet.of(CREATE, WRITE))) {
					sbc.write(bb);
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}

	private Path checkAndCreatePath(String root, String filename) {
		Path p = createPath(root, filename);
		try {
			Files.createDirectories(p.getParent());
		} catch (IOException e) {
		}
		return p;
	}

	private Path createPath(String root, String filename) {
		Path p = Paths.get("." + File.separator + root, filename);
		return p;
	}

	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.file.TorrentStorage#writeRelativePosition(byte[], int)
	 */
	@Override
	public void writeRelativePosition(byte data[], int position) {

		int position_absolute = 0;		
				
		// Check witch files
		for (TorrentFiles relativeFile : torrent.getTorrentFiles()) {
			if ((position_absolute + relativeFile.getLength()) >= position) {
				
				ByteBuffer bb;
				
				// Check 1st crossing files
				if (relativeFile.getLength() < data.length) {
					// Split and recursive call
					bb = ByteBuffer.wrap(data, 0, relativeFile.getLength());
					writeFile(position - position_absolute, relativeFile, bb);
					
					// Recursive call
					int lengthdatacross = data.length-relativeFile.getLength();
					byte datacross[] = new byte[lengthdatacross];
					System.arraycopy(data, relativeFile.getLength(), datacross, 0, lengthdatacross);
					writeRelativePosition(datacross, position+relativeFile.getLength()+1);
					return;
				} else {
					bb = ByteBuffer.wrap(data);
					writeFile(position - position_absolute, relativeFile, bb);
					return;
				}								
			}
			position_absolute += relativeFile.getLength();
		}
		
	}

	private void writeFile(int position, TorrentFiles relativeFile, ByteBuffer bb) {
		// Write into this files				
		Path p = this.createPath(torrent.getRootName(), relativeFile.getName());

		SeekableByteChannel sbc = null;
		try {
			sbc = Files.newByteChannel(p,EnumSet.of(WRITE));
			sbc.position(position);
			sbc.write(bb);					
			return;
		} catch (IOException e) {
			log.error(e);
		} 
		
		finally {
			try {
				sbc.close();
			} catch (IOException e) {
				log.error(e);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.file.TorrentStorage#readRelativePosition(int, int)
	 */
	@Override
	public byte[] readRelativePosition(int position, int size) {
		int position_absolute = 0;
		for (TorrentFiles files : torrent.getTorrentFiles()) {
			if ((position_absolute + files.getLength()) >= position) {
				// Write into this files
				ByteBuffer bb = ByteBuffer.allocate(size);
				Path p = this.createPath(torrent.getRootName(), files.getName());

				try (SeekableByteChannel sbc = Files.newByteChannel(p,
						EnumSet.of(READ))) {
					sbc.position(position - position_absolute);
					sbc.read(bb);
					bb.flip();
					bb.rewind();
					return bb.array();
				} catch (IOException e) {
					log.error(e);
				}
			}
			position_absolute += files.getLength();
		}
		
		return null;
	}
}
