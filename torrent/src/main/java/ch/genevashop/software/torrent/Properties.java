package ch.genevashop.software.torrent;

public class Properties {

	private String peer_id;
	private short port;

	public String getPeer_id() {
		return peer_id;
	}

	public byte[] getPeerIdInByte() {
		byte[] ret = new byte[20];
		System.arraycopy(peer_id.getBytes(), 0, ret, 0, 20);
		return ret;
	}

	public void setPeer_id(String peer_id) {
		this.peer_id = peer_id;
	}

	public short getPort() {
		return port;
	}

	public void setPort(short port) {
		this.port = port;
	}

}
