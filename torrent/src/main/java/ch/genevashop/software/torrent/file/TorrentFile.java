package ch.genevashop.software.torrent.file;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.nio.file.StandardOpenOption.READ;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.EnumSet;

import org.apache.log4j.Logger;

import ch.genevashop.software.tools.ByteTools;

public class TorrentFile {

	private static final int TORRENTDONETREESHOLD = 10;
	static final String TORRENT_PATH =".javatorrent";
	static final Logger log = Logger.getLogger(TorrentFile.class);
	private final String filename;
	private final int size;
	private byte data[];
	
	public TorrentFile(String filename, int size) {
		super();
		this.filename = filename;
		this.size = size;
		int sizedData = (size/8)+1;
		this.data = new byte[sizedData];		
		Arrays.fill(data, (byte) 0); // Set all byte to zero
	}

	public void updateState() {
		ByteBuffer bb = ByteBuffer.wrap(data);
		try (SeekableByteChannel sbc = Files.newByteChannel(
				checkAndCreatePath(), EnumSet.of(CREATE, WRITE))) {
			sbc.write(bb);
		} catch (IOException e) {
			log.error(e);
		}
	}

	private Path checkAndCreatePath() {
		Path p = Paths.get(System.getProperty("user.home") + File.separator + TORRENT_PATH, filename);
		try {
			Files.createDirectories(p.getParent());
		} catch (IOException e) {			
		}		
		return p;
	}
	
	public void readState() {
		ByteBuffer buff = ByteBuffer.allocate(size/8+1);
		try (SeekableByteChannel sbc = Files.newByteChannel(
				checkAndCreatePath(), EnumSet.of(CREATE, READ))) {
			sbc.read(buff);
			buff.flip();
			buff.rewind();
			data = buff.array();
		} catch (IOException e) {
			log.error(e);
		}
	}
	
	public void setBitAndUpdate(int pos, int val) {
		ByteTools.setBit(data, pos, val);
		this.updateState();
	}
	
	public int getBit(int pos) {
		return ByteTools.getBit(data, pos);
	}
	
	public boolean hasNoPieceDownloaded() {					
		int sum = 0;
		for (int index=0; index<this.getSize(); index++) {			
			int val = this.getBit(index);
			sum += val;
			if (sum > 4) return false;
		}
		return true;
	}
	
	public boolean isAlmostDone() {				
		int val = 0;
		for (int index=0; index<this.getSize(); index++) {
			val += this.getBit(index);			
		}
		
		if ((this.getSize()-val) < TORRENTDONETREESHOLD) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean isDone() {		
		int val = 0;
		for (int index=0; index<this.getSize(); index++) {
			val += this.getBit(index);			
		}
		
		if (this.getSize()== val) {
			return true;
		} else {
			return false;
		}
	}

	public int getSize() {
		return size;
	}
	
	
}
