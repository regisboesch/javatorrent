package ch.genevashop.software.torrent.tracker;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import ch.genevashop.software.tools.MapTools;
import ch.genevashop.software.torrent.metainfo.Digest;
import ch.genevashop.software.torrent.peer.Chunk;
import ch.genevashop.software.torrent.peer.Peer;
import ch.genevashop.software.torrent.policy.PeerPolicy;

public class TorrentDecision implements PeerPolicy{
	
	private static final int DECISIONTIMEOUT = 300;
	private static final int TIMEOUTREQUEST = 1000*30;
	private static final int MAXCHUNKPERPEER = 5;
	private final TorrentTracker torrentTracker;
	static final Logger log = Logger.getLogger(TorrentDecision.class);
	private Integer piece = null;
	private final Map<String, Integer> peersTimeout = new ConcurrentHashMap<>();
	
	public TorrentDecision(TorrentTracker torrentTracker) {
		super();
		this.torrentTracker = torrentTracker;
	}

	public TorrentTracker getTorrentTracker() {
		return torrentTracker;
	}

	private void reset() {
		piece = null;
		torrentTracker.getPeerCommunication().resetCurrentdata();
	}

	public void start() {

		while (true) {

			if (piece == null) {
				piece = torrentTracker.getPeerCommunication().chooseNextPiece();
				
				if (piece!=null) {
					log.debug("Choose piece according to piece policy: " + piece);				
					torrentTracker.getPeerCommunication().allocateNewPiece(piece);
				} else {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						log.error(e);
					}
					continue;
				}
			}

			// Get Set of peers for this piece
			List<String> peersChoosed = torrentTracker.getPeerCommunication()
					.getPeersForThisPiece(piece);

			// Send Interested to Set of peers
			for (String peer : peersChoosed) {
				Peer peerItem = torrentTracker.getPeer(peer);
				if ((peerItem != null) && (!peerItem.isPeerinterested())) {
					// Send Interested
					torrentTracker.getPeerCommunication()
							.sendInterestedMessageToPeer(peer);
					// Update state
					peerItem.setPeerinterested(true);
					this.increaseTimeout(peer);
				}
			}
			
			// Sort peers by timeout
			Map<String,Integer> peersSorted = choosePeers(peersTimeout);
			
			// Iterate over Chunk
			
			for (Chunk c: torrentTracker.getPeerCommunication().getChunkSet().values()) {
				if (!c.isDone()&&((System.currentTimeMillis()- c.getLastTime() ) > TIMEOUTREQUEST)) {
					
					log.debug("Timeout for chunk " + c.toString());
					this.increaseTimeout(c.getCurrentPeers());
				
					// Iterate over peers
					for (String peer : peersSorted.keySet()) {
						Peer peerItem = torrentTracker.getPeer(peer);
						int peerItemChunkAssigned = peerItem.getCurrentChunkAssigned();
						if ((peerItem != null) && !peerItem.isPeerchoked() && (peerItemChunkAssigned < MAXCHUNKPERPEER)) {
							
							// Set new peer for this chunk
							c.setCurrentPeers(peer);
							c.setLastTime(new Date().getTime());
							torrentTracker.getPeerCommunication().sendRequestMessageToPeer(peer, piece, c.getOffset(), c.getLength());
							peerItem.increaseChunkByOne();
						}
					}
				}
					
			}
			
			/*
			// TODO : Iterate over Chunk and after by peer
			for (String peer : peersSorted.keySet()) {
				Peer peerItem = torrentTracker.getPeer(peer);
				int peerItemChunkAssigned = peerItem.getCurrentChunkAssigned();
				if ((peerItem != null) && !peerItem.isPeerchoked() && (peerItemChunkAssigned < MAXCHUNKPERPEER)) {
					int chunkAssignedToThisPeer = MAXCHUNKPERPEER - peerItemChunkAssigned;
					
					int currentChunkAssigned = 0;
					for (Chunk c: torrentTracker.getPeerCommunication().getChunkSet().values()) {
						if (!c.isDone()&&((System.currentTimeMillis()- c.getLastTime() ) > TIMEOUTREQUEST)) {
							
							// Get old peer from chunk
							log.debug("Timeout for chunk " + c.toString());
							this.increaseTimeout(c.getCurrentPeers());
						
							// Set new peer for this chunk
							c.setCurrentPeers(peer);
							c.setLastTime(new Date().getTime());
							torrentTracker.getPeerCommunication().sendRequestMessageToPeer(peer, piece, c.getOffset(), c.getLength());
							peerItem.increaseChunkByOne();
							
							if (++currentChunkAssigned == chunkAssignedToThisPeer) {
								break;
							}
						}
					}
				}
			}
			*/
			
			// Check if we have all the Piece
			// - check hash of the piece
			// - write to File
			// - send Complete to PiecePolicy
			// - send HAVING to peers
			// - reset
			if (torrentTracker.getPeerCommunication().isPieceComplete(piece)) {
				log.debug("piece "+ piece  +" complete");
				
				// Check Hash
				byte[] dataChunk = torrentTracker.getPeerCommunication().getChunkData(piece);
				boolean hashValueTrue= checkHashFromPiece(dataChunk, piece);
				
				log.debug("Hash of piece "+ piece+ " is "+ hashValueTrue);
				if (!hashValueTrue) {
					// reset
					this.reset();
					continue;
				}
				
				// Write TO file
				torrentTracker.getTorrentStorage().writeRelativePosition(dataChunk, piece*torrentTracker.getTorrent().getPieceslength());
				
				// Send complete to PiecePolicy
				torrentTracker.getPeerCommunication().setPersonalPieceComplete(piece);
				
				// send HAVING to peers
				torrentTracker.getPeerCommunication().broadcastHaveMessage(piece);
				
				// reset
				this.reset();
			}
			
			// TODO : peer alive
			
			try {
				Thread.sleep(DECISIONTIMEOUT);
			} catch (InterruptedException e) {
				log.error(e);
			}

		}
	}

	private boolean checkHashFromPiece(byte[] temp, Integer piece)  {
		byte[] hashSHA1PieceFromTorrentMetaInfo = torrentTracker.getSHA1HashFromPiece(piece);
		
		try {
			byte[] hashSHA1PieceFromPeer = Digest.generateSHA1FromHash(temp, 0, temp.length);
			
			return Arrays.equals(hashSHA1PieceFromTorrentMetaInfo,hashSHA1PieceFromPeer);
		} catch (NoSuchAlgorithmException | IOException e) {
			log.warn(e);
			return false;
		}
		
	}

	private void increaseTimeout(String peers) {
		if (peers!=null) {
			if (!peersTimeout.containsKey(peers)) {
				peersTimeout.put(peers, 1);
			} else {
				peersTimeout.put(peers, peersTimeout.get(peers)+1);
			}
		}
	}
	
	@Override
	public Map<String, Integer> choosePeers(Map<String, Integer> peers) {
		return MapTools.sortByValueDesc(peers);
	}

}
