package ch.genevashop.software.torrent.peer.protocol;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.apache.log4j.Logger;

public class PeerMessage {	
	
	static final Logger log = Logger.getLogger(PeerMessage.class);
	
	private PeerMessageEnum peerMessageEnum;
	private byte[] data;

	public static PeerMessage parse(ByteBuffer bb) {
		
		PeerMessage p = new PeerMessage();			
		
		// Message ID : 1 byte
		byte messageID =  bb.get();
		
		// Assigning peerMEssageEnum		
		p.setPeerMessageEnum(PeerMessageEnum.getEnumByMessageID(messageID));
		
		if (p.getPeerMessageEnum().isPayload()) {
			int payloadLength = (int) (bb.array().length-1);
			p.initializeData(payloadLength);			
			bb.get(p.getData());
		}
		
		return p;
	}
	
	public static ByteBuffer createAliveMessage() {
		return createGenericPeerMessage(PeerMessageEnum.ALIVE);
	}
	
	public static ByteBuffer createInterestedMessage() {		
		return createGenericPeerMessage(PeerMessageEnum.INTERESTED);
	}	
	
	public static ByteBuffer createNotInterestedMessage() {		
		return createGenericPeerMessage(PeerMessageEnum.NOTINTERESTED);
	}
	
	public static ByteBuffer createChokedMessage() {		
		return createGenericPeerMessage(PeerMessageEnum.CHOKE);
	}
	
	public static ByteBuffer createUnChokedMessage() {		
		return createGenericPeerMessage(PeerMessageEnum.UNCHOKE);
	}
	
	public static ByteBuffer createRequestMessage(int index, int begin, int length) {		
		ByteBuffer bb = ByteBuffer.allocate(PeerMessageEnum.REQUEST.getBufferAllocate());
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(PeerMessageEnum.REQUEST.getFixedSize());
		bb.put(PeerMessageEnum.REQUEST.getMessageId());
		bb.putInt(index);
		bb.putInt(begin);
		bb.putInt(length);
		bb.rewind();		
		return bb;
	}
	
	public static ByteBuffer createHaveMessage(int index) {
		ByteBuffer bb = ByteBuffer.allocate(PeerMessageEnum.HAVE.getBufferAllocate());
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(PeerMessageEnum.HAVE.getFixedSize());
		bb.put(PeerMessageEnum.HAVE.getMessageId());
		bb.putInt(index);
		bb.rewind();		
		return bb;
	}
	
	public static ByteBuffer createBitfieldMessage(byte data[]) {
		ByteBuffer bb = ByteBuffer.allocate(PeerMessageEnum.BITFIELD.getBufferAllocate());
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(PeerMessageEnum.BITFIELD.getFixedSize()+data.length);
		bb.put(PeerMessageEnum.BITFIELD.getMessageId());
		bb.put(data);
		bb.rewind();		
		return bb;
	}
	
	public static ByteBuffer createPieceMessage(int index, int begin, byte data[]) {
		ByteBuffer bb = ByteBuffer.allocate(PeerMessageEnum.PIECE.getBufferAllocate());
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(PeerMessageEnum.PIECE.getFixedSize()+data.length);
		bb.put(PeerMessageEnum.PIECE.getMessageId());
		bb.putInt(index);
		bb.putInt(begin);
		bb.put(data);
		bb.rewind();		
		return bb;
	}
	
	public static ByteBuffer createCancelMessage(int index, int begin, int length) {
		ByteBuffer bb = ByteBuffer.allocate(PeerMessageEnum.CANCEL.getBufferAllocate());
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(PeerMessageEnum.CANCEL.getFixedSize());
		bb.put(PeerMessageEnum.CANCEL.getMessageId());
		bb.putInt(index);
		bb.putInt(begin);
		bb.putInt(length);
		bb.rewind();		
		return bb;
	}
	
	private static ByteBuffer createGenericPeerMessage(PeerMessageEnum p) {
		ByteBuffer bb = ByteBuffer.allocate(p.getBufferAllocate());
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(p.getFixedSize());
		if (p.getFixedSize() > 0) {
			bb.put(p.getMessageId());
		}
		bb.rewind();		
		return bb;
	}

	public PeerMessageEnum getPeerMessageEnum() {
		return peerMessageEnum;
	}

	public void setPeerMessageEnum(PeerMessageEnum peerMessageEnum) {
		this.peerMessageEnum = peerMessageEnum;
	}

	public void initializeData(int size) {
		data = new byte[size];
	}
	
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
		
}
