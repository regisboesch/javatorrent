package ch.genevashop.software.torrent.peer;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ch.genevashop.software.torrent.metainfo.Torrent;

public class PeerCommunicatorInMemoryData implements PeerCommunicatorData {
	
	public static final int CHUNKSIZE = 16384;
	private final Map<Integer, Chunk> chunkSet = new ConcurrentHashMap<>(); // Offset, Chunk
	private final Torrent torrent;
	private int currentPiece;
	
	public PeerCommunicatorInMemoryData(Torrent torrent) {
		super();
		this.torrent = torrent;
	}

	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#getTorrent()
	 */
	@Override
	public Torrent getTorrent() {
		return torrent;
	}

	// TODO : to change !!
	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#getChunkSet()
	 */
	@Override
	public Map<Integer, Chunk> getChunkSet() {
		return chunkSet;
	}
	
	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#allocateNewPiece(int)
	 */
	@Override
	public void allocateNewPiece(int piece) {

		this.currentPiece = piece;
		
		// Split piece in size of 16KB
		int pieceLength = getTorrent().getPieceslength();		
		int numberofchunk = pieceLength / CHUNKSIZE;
				
		// If last piece
		if (getTorrent().getNumberOfPieces() == (piece+1)) {
			int totalsize = getTorrent().computeTotalLength();
			int lastpiecesize = totalsize % pieceLength;
						
			if (lastpiecesize>0) {
				
				numberofchunk = lastpiecesize / CHUNKSIZE;
			
				// Last chunk size
				int lastChunkSize = lastpiecesize % CHUNKSIZE;
				if (lastChunkSize > 0) {
					numberofchunk++;
				}
				
				for (int index = 0; index < numberofchunk; index++) {
					
					// Last chunk
					Chunk chunk = null;
					
					if (((index+1) == numberofchunk) && (lastChunkSize > 0)) {
						chunk = new Chunk(piece, index * CHUNKSIZE, lastChunkSize);
					} else {
						chunk = new Chunk(piece, index * CHUNKSIZE, CHUNKSIZE);
						
					}					
					chunkSet.put(new Integer(index), chunk);
				}
				
			} else {
				// Normal piece size
				for (int index = 0; index < numberofchunk; index++) {
					Chunk chunk = new Chunk(piece, index * CHUNKSIZE, CHUNKSIZE);
					chunkSet.put(new Integer(index), chunk);
				}
			}
		} else if (piece >= getTorrent().getNumberOfPieces()) {
			resetCurrentdata();
		}	
		else {
		
			// Normal piece size
			for (int index = 0; index < numberofchunk; index++) {
				Chunk chunk = new Chunk(piece, index * CHUNKSIZE, CHUNKSIZE);
				chunkSet.put(new Integer(index), chunk);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#getChunkData(int)
	 */
	@Override
	public byte[] getChunkData() {
		
		// Get total size of the Chunk
		int totalSize = 0;
		if (chunkSet != null) {
			for (Chunk c : chunkSet.values()) {
				totalSize += c.getData().length;
			}
		}
		
		// Merge chunk data
		if (totalSize>0) {
			ByteBuffer bb  = ByteBuffer.allocate(totalSize);
			
			for (Chunk c : chunkSet.values()) {
				bb.put(c.getData());
			}			
			return bb.array();
			
		} else return null;
		
		
	}
	
	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#isPieceComplete(int)
	 */
	@Override
	public boolean isPieceComplete() {
		boolean result = true;
		if (chunkSet != null) {
			for (Chunk c : chunkSet.values()) {
				result &= c.isDone();
			}
		} else {
			result = false;
		}
		return result;
	}

	// TODO : check if current piece is still valid
	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#writeToPiece(byte[], int, int)
	 */
	@Override
	public void writeToPiece(byte[] data, int offset) {
		
		if (data != null && (data.length > 0)) {
			
			int indexofchunk = offset / CHUNKSIZE;
			
			if (!chunkSet.isEmpty() && chunkSet.containsKey(indexofchunk)) {
				
				// Write data
				chunkSet.get(indexofchunk).setData(data);
				
				// Update chunk state
				chunkSet.get(indexofchunk).setDone(true);	
			}
		}
	}

	/* (non-Javadoc)
	 * @see ch.genevashop.software.torrent.peer.PeerCommunicatorData#resetCurrentdata()
	 */
	@Override
	public void resetCurrentdata() {		
		chunkSet.clear();
	}

	@Override
	public int getCurrentPieceIndex() {		
		return currentPiece;
	}
}