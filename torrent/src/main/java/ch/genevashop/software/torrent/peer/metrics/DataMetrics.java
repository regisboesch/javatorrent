package ch.genevashop.software.torrent.peer.metrics;

public class DataMetrics {
	private int lastUploadRatio;
	private int lastDowndloadRatio;

	public DataMetrics() {
		super();
		this.lastUploadRatio = 0;
		this.lastDowndloadRatio = 0;
	}

	public DataMetrics(int lastUploadRatio, int lastDowndloadRatio) {
		super();
		this.lastUploadRatio = lastUploadRatio;
		this.lastDowndloadRatio = lastDowndloadRatio;
	}

	public int getLastUploadRatio() {
		return lastUploadRatio;
	}

	public void setLastUploadRatio(int lastUploadRatio) {
		this.lastUploadRatio = lastUploadRatio;
	}

	public int getLastDowndloadRatio() {
		return lastDowndloadRatio;
	}

	public void setLastDowndloadRatio(int lastDowndloadRatio) {
		this.lastDowndloadRatio = lastDowndloadRatio;
	}

	public String toString() {
		return (" - D:" + lastDowndloadRatio + " / U:" + lastUploadRatio);
	}

	public void add(DataMetrics anotherDataMetrics) {
		this.lastDowndloadRatio += anotherDataMetrics.getLastDowndloadRatio();
		this.lastUploadRatio += anotherDataMetrics.getLastDowndloadRatio();
	}
}