package ch.genevashop.software.torrent.peer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.peer.protocol.PeerMessage;
import ch.genevashop.software.torrent.peer.protocol.PeerMessageEnum;
import ch.genevashop.software.torrent.policy.PiecePolicy;
import ch.genevashop.software.torrent.policy.RarestFirstAlgorithm;
import ch.genevashop.software.torrent.tracker.TorrentTracker;

public class PeerCommunicator {

	static final Logger log = Logger.getLogger(PeerCommunicator.class);
	private final TorrentTracker tracker;
	private final Map<String, PeerSelector> peerSelectorMap = new ConcurrentHashMap<>(); // PeerID,
																							// PeerSelector
	private final Map<Integer, AtomicInteger> piecesMap = new ConcurrentHashMap<>(); // Contains
																						// the
																						// number
																						// of
																						// occurrence
																						// of
																						// each
																						// pieces
																						// summed
																						// by
																						// peer
	private PiecePolicy piecePolicy = null;
	
	private final PeerCommunicatorData peerCommunicatorData;

	public PeerCommunicator(TorrentTracker tracker) {
		super();
		this.tracker = tracker;
		this.peerCommunicatorData = new PeerCommunicatorInMemoryData(tracker.getTorrent());
	}

	public void connectPeerList(Map<String, Peer> map) {
		log.debug("Start connecting peers");
		int interestSet = SelectionKey.OP_CONNECT | SelectionKey.OP_READ
				| SelectionKey.OP_WRITE;

		for (Map.Entry<String, Peer> entry : map.entrySet()) {
			String keyPeer = entry.getKey();
			Peer peer = entry.getValue();

			if (peer.getInternalState() == PeerState.NOTCONNECTED) {
				SocketChannel socketChannel;
				try {
					socketChannel = SocketChannel.open();
				} catch (IOException e) {
					log.error("Unable to open socket", e);
					continue;
				}
				try {
					socketChannel.configureBlocking(false);
				} catch (IOException e) {
					log.error("Unable to set blocking to false", e);
				}

				PeerSelector peerSelector = new PeerSelector(this);
				try {
					SelectionKey sk = socketChannel.register(
							peerSelector.getSelector(), interestSet);
					sk.attach(keyPeer);
				} catch (ClosedChannelException e) {
					log.error("Unable to register", e);
				}

				// Launch PeerSelecotr Thread
				peerSelectorMap.put(keyPeer, peerSelector);
				new Thread(peerSelector).start();

				try {
					socketChannel.connect(peer.getAddress());
				} catch (IOException e) {
					log.error("Unable to connect", e);
				}
			}
		}
	}

	public void launchPiecePolicyAlgorithm() {
		piecePolicy = new RarestFirstAlgorithm();
	}

	public void incrementPieceFromPeers(Integer numberPiece) {
		if (piecesMap.containsKey(numberPiece)) {
			piecesMap.get(numberPiece).incrementAndGet();
		} else {
			piecesMap.put(numberPiece, new AtomicInteger(1));
		}
	}

	public Integer chooseNextPiece() {
		Map<Integer, Integer> convertedMap = new HashMap<>();

		for (Map.Entry<Integer, AtomicInteger> entry : piecesMap.entrySet()) {
			Integer key = entry.getKey();
			AtomicInteger value = entry.getValue();
			
			// Put only piece not completed
			if (tracker.getTorrentFile().getBit(key)==0) {
				convertedMap.put(key, value.get());
			}
		}

		return piecePolicy.getNextPiece(convertedMap, tracker.getTorrentFile());
	}

	public void allocateNewPiece(int piece) {
		peerCommunicatorData.allocateNewPiece(piece);
	}
	
	public byte[] getChunkData(int piece) {			
		return peerCommunicatorData.getChunkData();
	}
	
	public boolean isPieceComplete(int piece) {
		return peerCommunicatorData.isPieceComplete();
	}

	public void writeToPiece(byte[] data, int piece, int offset) {		
		if (peerCommunicatorData.getCurrentPieceIndex()==piece) {
			peerCommunicatorData.writeToPiece(data, offset);
		}
	}

	public void resetCurrentdata() {		
		peerCommunicatorData.resetCurrentdata();
	}

	public void setPersonalPieceComplete(Integer piece) {
		piecePolicy.setPieceComplete(piece);			
		tracker.getTorrentFile().setBitAndUpdate(piece, 1);
	}

	public TorrentTracker getTracker() {
		return tracker;
	}

	public Map<Integer, Chunk> getChunkSet() {
		return peerCommunicatorData.getChunkSet();
	}

	public List<String> getPeersForThisPiece(Integer piece) {
		LinkedList<String> peersChoosed = new LinkedList<>();
		for (Entry<String, PeerSelector> entry : peerSelectorMap.entrySet()) {
			String key = entry.getKey();
			PeerSelector value = entry.getValue();
			if (value.hasPiece(piece)) {
				peersChoosed.add(key);
			}
		}
		Collections.shuffle(peersChoosed);
		return peersChoosed;
	}

	public void sendInterestedMessageToPeer(String peerID) {
		// Create Message
		log.debug("Send [" + PeerMessageEnum.INTERESTED.name() + "] to peer"
				+ peerID);
		ByteBuffer bb = PeerMessage.createInterestedMessage();
		peerSelectorMap.get(peerID).addMessageToSend(bb);
	}

	public void sendRequestMessageToPeer(String peerID, int index, int begin,
			int length) {
		log.debug("Send [" + PeerMessageEnum.REQUEST.name() + "] piece ["+index+"] begin ["+begin+"] length ["+length+"] to peer"
				+ peerID);
		ByteBuffer bb = PeerMessage.createRequestMessage(index, begin, length);
		peerSelectorMap.get(peerID).addMessageToSend(bb);
	}
	
	public void broadcastHaveMessage(int index) {
		for (Entry<String, PeerSelector> entry : peerSelectorMap.entrySet()) {
			String peerID = entry.getKey();
			PeerSelector peer = entry.getValue();
			this.sendHaveMessage(peerID, peer, index);
		}
	}
	
	private void sendHaveMessage(String peerID, PeerSelector peer, int index) {
		log.debug("Send [" + PeerMessageEnum.HAVE.name() + "] piece ["+index+"]  to peer" + peerID);
		ByteBuffer bb = PeerMessage.createHaveMessage(index);
		peer.addMessageToSend(bb);
	}
}
