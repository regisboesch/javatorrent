package ch.genevashop.software.torrent.policy;

import java.util.Map;

public interface PeerPolicy {
	public Map<String, Integer> choosePeers(Map<String, Integer> peers);
}
