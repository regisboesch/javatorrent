package ch.genevashop.software.torrent.benconding;

import java.util.Arrays;

public class BString extends DataTypePosition implements DataType<String> {

	public String read(byte dataIn[], int position) throws BReadException {

		internalPosition = position;

		int firstcolon = 0;

		while (((char) dataIn[position] != BDELIMITER)
				&& (position < dataIn.length)) {
			position++;
		}

		firstcolon = position;

		int lengthvalue = new Integer(new String(Arrays.copyOfRange(dataIn,
				internalPosition, firstcolon)));

		int currentvalueposition = firstcolon + 1;

		internalPosition = currentvalueposition + lengthvalue;

		return new String(Arrays.copyOfRange(dataIn, currentvalueposition,
				currentvalueposition + lengthvalue));

	}
	
	public byte[] readByte(byte dataIn[], int position) throws BReadException {

		internalPosition = position;

		int firstcolon = 0;

		while (((char) dataIn[position] != BDELIMITER)
				&& (position < dataIn.length)) {
			position++;
		}

		firstcolon = position;

		int lengthvalue = new Integer(new String(Arrays.copyOfRange(dataIn,
				internalPosition, firstcolon)));

		int currentvalueposition = firstcolon + 1;

		internalPosition = currentvalueposition + lengthvalue;

		return Arrays.copyOfRange(dataIn, currentvalueposition,
				currentvalueposition + lengthvalue);

	}

	public byte[] write(String dataout) throws BWriteException {
		// TODO Auto-generated method stub
		return null;
	}

}
