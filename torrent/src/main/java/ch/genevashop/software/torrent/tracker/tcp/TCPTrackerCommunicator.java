package ch.genevashop.software.torrent.tracker.tcp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.Properties;
import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.tracker.Status;
import ch.genevashop.software.torrent.tracker.TrackerCommunicator;

public class TCPTrackerCommunicator extends TrackerCommunicator {

	static final Logger log = Logger.getLogger(TCPTrackerCommunicator.class);

	public TCPTrackerCommunicator(Torrent torrent, Properties properties) {
		super(torrent, properties);
	}

	@Override
	public boolean connect(URI uri, int left) {
		this.setStatus(Status.STARTED);
		this.setLeft(left);
		try {
			return connectHTTP(uri);
		} catch (Exception e) {
			log.warn("Unnable to connect to "+ uri, e);
			return false;
		}
	}

	private boolean connectHTTP(URI uri) throws Exception {
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet request = new HttpGet(uri);

		// Add parameters
		uri = buildAnnounceURL(uri.toURL()).toURI();

		((HttpRequestBase) request).setURI(uri);

		// Add ResposeHandler
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

			public String handleResponse(final HttpResponse response)
					throws ClientProtocolException, IOException {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();
					return entity != null ? EntityUtils.toString(entity) : null;
				} else {
					throw new ClientProtocolException(
							"Unexpected response status: " + status);
				}
			}

		};

		// Execute request
		String responseBody = httpclient.execute(request, responseHandler);

		// Process response
		processAnnounceResponse(responseBody.getBytes());

		return true;
	}

	private void processAnnounceResponse(byte[] bytes) {
		TrackerParser tp = new TrackerParser(this, bytes);
		tp.parse(0);
		tp.processCompactPeersList(this.getPeers());
	}

	private URL buildAnnounceURL(URL trackerAnnounceURL)
			throws UnsupportedEncodingException, MalformedURLException {
		String base = trackerAnnounceURL.toString();
		StringBuilder url = new StringBuilder(base);
		url.append(base.contains("?") ? "&" : "?")
				.append("info_hash=")
				.append(getTorrent().getSHA1infoHashForGet())
				.append("&peer_id=")
				.append(URLEncoder
						.encode(getProperties().getPeer_id(), "UTF-8"))
				.append("&port=").append(getProperties().getPort())
				.append("&uploaded=").append(this.getUploaded())
				.append("&downloaded=").append(this.getDownloaded())
				.append("&compact=1&left=").append(this.getLeft())
				.append("&event=").append(getStatus().name().toLowerCase());

		return new URL(url.toString());
	}
}
