package ch.genevashop.software.torrent.peer.protocol;

public enum PeerMessageEnum {
	
	ALIVE((byte)0x00, false, 4, 0),
	CHOKE((byte)0x00, false, 5, 1),
	UNCHOKE((byte)0x01, false, 5, 1),
	INTERESTED((byte)0x02, false, 5, 1),
	NOTINTERESTED((byte)0x03, false, 5, 1),
	HAVE((byte)0x04, true, 9, 5),
	BITFIELD((byte)0x05, true, 5, 1),
	REQUEST((byte)0x06, true, 17, 13),
	PIECE((byte)0x07, true, 13, 9),
	CANCEL((byte)0x08, false, 17, 13),
	UNDEFINED((byte)0xFF,false, 0, 0);
	
	private final byte messageId;	
	private final boolean payload;
	private final int bufferAllocate;
	private final int fixedSize;
	
	private PeerMessageEnum(byte messageId, boolean payload, int bufferAllocate, int fixedSize) {
		this.messageId = messageId;		
		this.payload = payload;
		this.bufferAllocate = bufferAllocate;
		this.fixedSize = fixedSize;
	}
	
	public byte getMessageId() {
		return messageId;
	}
	
	public boolean isPayload() {
		return payload;
	}			
		
	
	public int getFixedSize() {
		return fixedSize;
	}
		

	public int getBufferAllocate() {
		return bufferAllocate;
	}

	public static PeerMessageEnum getEnumByMessageID(byte messageID) {
		for (PeerMessageEnum p : PeerMessageEnum.values()) {
			if (messageID == p.getMessageId()) {
				return p;
			}
		}		
		return PeerMessageEnum.UNDEFINED;
	}
}
