package ch.genevashop.software.torrent;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;

import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.metainfo.FileReader;
import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.tracker.TorrentTracker;

public class Starter {

	private static final short DEFAULTPORT = 6882;
	private Properties properties;
	static final Logger log = Logger.getLogger(Starter.class);

	public void initialize() {

		// Create application properties
		properties = new Properties();

		// Compute random uuid for the peer_id
		computeUUID(properties);

		// Assign Port
		properties.setPort(DEFAULTPORT);
	}

	public void start(String torrentFilename) {

		// Read torrent file
		log.debug("Read torrent file : " + torrentFilename);

		byte data[] = null;
		FileReader fr = new FileReader();
		try {
			data = fr.readFromFile(torrentFilename);
		} catch (FileNotFoundException e) {
			log.error("File not found", e);
			System.exit(1);
		}

		// Parse torrent
		Torrent torrent = new Torrent();
		torrent.parseTorrent(data);
		
		// Create Tracker
		TorrentTracker tracker = new TorrentTracker(properties, torrent);

		// Start Session
		try {
			tracker.startSession();
		} catch (URISyntaxException | IOException e) {
			log.error("Somethign wrong in request :", e);
		}
	}

	private void computeUUID(Properties properties) {
		properties.setPeer_id("-TO0042-"+UUID.randomUUID().toString().substring(0,12));		
		log.debug("Peer iD : " + properties.getPeer_id());
	}

	public static void main(String[] args) {
		Starter starter = new Starter();
		starter.initialize();

		starter.start(args[0]);
	}
}
