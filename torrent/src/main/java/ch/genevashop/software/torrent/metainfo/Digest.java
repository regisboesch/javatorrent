package ch.genevashop.software.torrent.metainfo;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Digest {

	public static byte[] generateSHA1FromHash(byte[] data, int start, int end)
			throws NoSuchAlgorithmException, IOException {
		MessageDigest sha1 = MessageDigest.getInstance("SHA-1");		
		sha1.update(data, start, end);
		byte[] hash = sha1.digest();		
		return hash;		
	}
}
