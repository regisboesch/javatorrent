package ch.genevashop.software.torrent.common;

import ch.genevashop.software.torrent.benconding.BInteger;
import ch.genevashop.software.torrent.benconding.BReadException;
import ch.genevashop.software.torrent.benconding.BString;

public abstract class CommonParser {

	protected final static char LISTCHARSTART = 'l';
	protected final static char LISTCHAREND = 'e';
	protected final static char DICTIONARYCHARSTART = 'd';
	protected final static char DICTIONARYCHAREND = 'e';
	protected byte[] data;
	protected int level = 0;
	
	public abstract int parse(int position);
	protected abstract int parseIntegerOrString(int position, BString b,
			BInteger i) throws BReadException;

	protected String getLevelFormat() {
		String r = "";
		for (int index=0; index < level; index++) {
			r+= "  ";
		}
		return r;
	}
	
	public byte[] getData() {
		return data;
	}
	
	public void setData(byte[] data) {
		this.data = data;
	}
	
	
}
