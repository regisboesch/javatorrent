package ch.genevashop.software.torrent.peer;

public enum PeerState {
	NOTCONNECTED,
	CONNECTED,
	HANDSHAKE_SEND,
	HANDSHAKE_RECEIVED
}
