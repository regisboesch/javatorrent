package ch.genevashop.software.torrent.peer.metrics;

import java.util.concurrent.atomic.AtomicInteger;

public class PeerMetrics {

	private long lastTime;
	private AtomicInteger upload;
	private AtomicInteger download;
	private int lastUploadRatio;
	private int lastDowndloadRatio;
	
	{
		lastTime = System.currentTimeMillis();		
		upload = new AtomicInteger(0);
		download = new AtomicInteger(0);
		lastUploadRatio = 0;
		lastDowndloadRatio = 0;
	}
	
	public void updateUpload(int size) {
		upload.addAndGet(size);		
	}
	
	public void updateDownload(int size) {
		download.addAndGet(size);
	}
	
	public void computeRatio() {
		long currentTime = System.currentTimeMillis();
		long deltaTime = currentTime - lastTime;
		
		if (deltaTime==0L) {
			return;
		}
		
		lastUploadRatio = upload.getAndSet(0);
		lastDowndloadRatio = download.getAndSet(0);
		
		lastTime = currentTime;
	}

	public int getLastUploadRatio() {
		return lastUploadRatio;
	}

	public void setLastUploadRatio(int lastUploadRatio) {
		this.lastUploadRatio = lastUploadRatio;
	}

	public int getLastDowndloadRatio() {
		return lastDowndloadRatio;
	}

	public void setLastDowndloadRatio(int lastDowndloadRatio) {
		this.lastDowndloadRatio = lastDowndloadRatio;
	}

	public long getLastTime() {
		return lastTime;
	}		
		
}
