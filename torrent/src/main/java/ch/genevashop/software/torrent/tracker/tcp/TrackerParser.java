package ch.genevashop.software.torrent.tracker.tcp;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.benconding.BInteger;
import ch.genevashop.software.torrent.benconding.BReadException;
import ch.genevashop.software.torrent.benconding.BString;
import ch.genevashop.software.torrent.common.CommonParser;
import ch.genevashop.software.torrent.peer.Peer;

/**
 * Tracker response parser
 * 
 * Tracker Response The tracker responds with "text/plain" document consisting
 * of a bencoded dictionary with the following keys:
 * 
 * failure reason: If present, then no other keys may be present. The value is a
 * human-readable error message as to why the request failed (string).
 * 
 * warning message: (new, optional) Similar to failure reason, but the response
 * still gets processed normally. The warning message is shown just like an
 * error.
 * 
 * interval: Interval in seconds that the client should wait between sending
 * regular requests to the tracker
 * 
 * min interval: (optional) Minimum announce interval. If present clients must
 * not reannounce more frequently than this.
 * 
 * tracker id: A string that the client should send back on its next
 * announcements. If absent and a previous announce sent a tracker id, do not
 * discard the old value; keep using it.
 * 
 * complete: number of peers with the entire file, i.e. seeders (integer)
 * 
 * incomplete: number of non-seeder peers, aka "leechers" (integer)
 * 
 * peers: (dictionary model) The value is a list of dictionaries, each with the
 * following keys: peer id: peer's self-selected ID, as described above for the
 * tracker request (string) ip: peer's IP address either IPv6 (hexed) or IPv4
 * (dotted quad) or DNS name (string) port: peer's port number (integer)
 * 
 * peers: (binary model) Instead of using the dictionary model described above,
 * the peers value may be a string consisting of multiples of 6 bytes. First 4
 * bytes are the IP address and last 2 bytes are the port number. All in network
 * (big endian) notation.
 * 
 * @author regis
 * 
 */
public class TrackerParser extends CommonParser {
	
	private final TCPTrackerCommunicator tracker;
	final static String COMPLETE = "complete";
	final static String INCOMPLETE = "incomplete";
	final static String INTERVAL = "interval";
	final static String MININTERVAL = "min interval";
	final static String PEERS = "peers";
	static final Logger log = Logger.getLogger(TrackerParser.class);

	private boolean inKeyComplete;
	private boolean inKeyIncomplete;
	private boolean inKeyInterval;
	private boolean inKeyMinInterval;
	private boolean inKeyPeers;

	public TrackerParser(TCPTrackerCommunicator tracker, byte[] data) {
		super();
		this.tracker = tracker;
		this.data = data;
	}

	@Override
	public int parse(int position) {
		BString b = new BString();
		BInteger i = new BInteger();

		if ((char) data[position] == DICTIONARYCHARSTART) {

			// Parse dictionary
			position += 1;
			log.debug(getLevelFormat() + "<Dictionary Start>");
			level += 1;
			while ((char) data[position] != DICTIONARYCHAREND) {

				try {
					// Key
					String text_key = b.read(data, position);
					log.debug(getLevelFormat() + "Key : " + text_key);
					position = b.getActualPosition(); // Get position after key

					// Values
					inKeyComplete = false;
					inKeyIncomplete = false;
					inKeyInterval = false;
					inKeyMinInterval = false;
					inKeyPeers = false;

					if (text_key.compareTo(COMPLETE) == 0) {
						inKeyComplete = true;
					} else if (text_key.compareTo(INCOMPLETE) == 0) {
						inKeyIncomplete = true;
					} else if (text_key.compareTo(INTERVAL) == 0) {
						inKeyInterval = true;
					} else if (text_key.compareTo(MININTERVAL) == 0) {
						inKeyMinInterval = true;
					} else if (text_key.compareTo(PEERS) == 0) {
						inKeyPeers = true;						
					}

					// By default this is a Integer or String
					position = parseIntegerOrString(position, b, i);
				} catch (BReadException e) {
					log.error("Error on BString", e);
				}

			}

			// End of dictionary
			level -= 1;
			log.debug(getLevelFormat() + "<Dictionary End> at " + position);
			position += 1;

			return position;
		}
		return position;
	}

	@Override
	protected int parseIntegerOrString(int position, BString b, BInteger i)
			throws BReadException {

		if (i.isInteger(data, position)) {
			Integer int_value = i.read(data, position);
			log.debug(getLevelFormat() + "Integer : " + int_value);

			if (inKeyComplete) {
				tracker.setComplete(int_value);
			} else if (inKeyIncomplete) {
				tracker.setIncomplete(int_value);
			} else if (inKeyInterval) {
				tracker.setIntervalInSecond(int_value);
			} else if (inKeyMinInterval) {
				tracker.setMinIntervalInSecond(int_value);
			}
			position = i.getActualPosition();
		} else {
			if (inKeyPeers) {
				tracker.setPeers(b.readByte(data, position));
				position = b.getActualPosition();
			}
		}

		return position;
	}

	public TCPTrackerCommunicator getTracker() {
		return tracker;
	}

	public void processCompactPeersList(byte[] dataIn) {
		log.debug("Process peers list");

		ByteBuffer peers = ByteBuffer.wrap(dataIn);

		for (int i = 0; i < dataIn.length / 6; i++) {
			byte[] ipBytes = new byte[4];
			peers.get(ipBytes);
			InetAddress ip;
			try {
				ip = InetAddress.getByAddress(ipBytes);

				int port = (0xFF & (int) peers.get()) << 8
						| (0xFF & (int) peers.get());

				InetSocketAddress ia = new InetSocketAddress(ip, port);
				Peer newPeer = new Peer(ia);
				tracker.getPeerslist().put(newPeer.toString(), newPeer);

				log.debug("Add peer :" + ia.toString());
			} catch (UnknownHostException e) {
				log.error("Error on address", e);
			}

		}

	}

}
