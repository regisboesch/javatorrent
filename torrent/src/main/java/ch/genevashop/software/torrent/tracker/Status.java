package ch.genevashop.software.torrent.tracker;

public enum Status {
	WAITING(0), STARTED(2), STOPPED(3), COMPLETED(1);

	private final int event;

	private Status(int event) {
		this.event = event;
	}

	public int getEvent() {
		return event;
	}

}
