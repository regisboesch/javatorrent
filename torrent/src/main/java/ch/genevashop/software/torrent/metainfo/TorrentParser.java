package ch.genevashop.software.torrent.metainfo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.benconding.BInteger;
import ch.genevashop.software.torrent.benconding.BReadException;
import ch.genevashop.software.torrent.benconding.BString;
import ch.genevashop.software.torrent.common.CommonParser;

public class TorrentParser extends CommonParser {

	/**
	 * Torrent Parser
	 */
	
	final static String INFOKEY = "info";
	final static String ANNOUNCE = "announce";
	final static String ANNOUNCELIST = "announce-list";
	final static String NAME = "name";
	final static String LENGTH = "length";
	final static String PIECELENGTH = "piece length";
	final static String PIECES = "pieces";
	final static String FILES = "files";
	final static String PATH = "path";
	static final Logger log = Logger.getLogger(TorrentParser.class);

	private int info_hash_pos = 0;
	private final Torrent torrent;
	private boolean inKeyAnnounce;
	private boolean inKeyAnnounceList;
	private boolean inKeyName;
	private boolean inKeyLength;
	private boolean inKeyPieceLength;
	private boolean inKeyPieces;
	private boolean inKeyPath;
	private boolean multipleFiles;
	private int filesIndex = 0;	
	
	public TorrentParser(byte data[], Torrent torrent) {
		super();
		this.data = data;
		this.torrent = torrent;
	}

	public int parse(int position) {
		BString b = new BString();
		BInteger i = new BInteger();

		if ((char) data[position] == LISTCHARSTART) {
			// Parse list
			position += 1;
			log.debug(getLevelFormat() + "<List Start> at " + position);			
			level +=1;
			
			while ((char) data[position] != LISTCHAREND) {

				// Check if list as value or dictionary or value
				if ((char) data[position] == LISTCHARSTART) {
					position = parse(position);
				} else if ((char) data[position] == DICTIONARYCHARSTART) {
					position = parse(position);				
				} 
				
				else {
					// String or Integer
					try {
						position = parseIntegerOrString(position, b, i);
					} catch (BReadException e) {
						log.error("Error on parsing BString or BInteger");
					}
				}
			}

			level-=1;
			log.debug(getLevelFormat() + "<List End> at " + position);
			
			position += 1;
			return position;
		}

		else if ((char) data[position] == DICTIONARYCHARSTART) {

			// Parse dictionary
			position += 1;
			log.debug(getLevelFormat() + "<Dictionary Start>");
			level+=1;
			while ((char) data[position] != DICTIONARYCHAREND) {

				// Parsing key:value
				try {

					// Key
					String text_key = b.read(data, position);
					log.debug(getLevelFormat() + "Key : " + text_key);
					position = b.getActualPosition(); // Get position after key

					// Save state key position
					inKeyAnnounce = false;
					inKeyAnnounceList = false;
					inKeyName = false;
					inKeyLength = false;
					inKeyPieceLength = false;
					inKeyPieces = false;					
					inKeyPath = false;
					
					if (text_key.compareTo(INFOKEY) == 0) {
						info_hash_pos = position;
					} else if (text_key.compareTo(ANNOUNCE) == 0) {
						inKeyAnnounce = true;
					} else if (text_key.compareTo(ANNOUNCELIST) == 0) {
						inKeyAnnounceList = true;
					} else if (text_key.compareTo(LENGTH) == 0) {
						inKeyLength = true;
					} else if (text_key.compareTo(PIECELENGTH) == 0) {
						inKeyPieceLength = true;
					} else if (text_key.compareTo(NAME) == 0) {
						inKeyName = true;
					} else if (text_key.compareTo(PIECES) == 0) {
						inKeyPieces = true;
					} else if (text_key.compareTo(FILES) == 0) {
						multipleFiles = true;
					} else if (text_key.compareTo(PATH) == 0) {
						inKeyPath = true;
					}

					// Check if list as value
					if ((char) data[position] == LISTCHARSTART) {
						position = parse(position);
						continue;
					}

					// Check if dictionary as value
					else if ((char) data[position] == DICTIONARYCHARSTART) {
						// recursive parsing
						position = parse(position);
						continue;
					}

					// By default this is a Integer or String
					position = parseIntegerOrString(position, b, i);

				} catch (BReadException e) {
					log.error("Error on BString", e);
				}
			}

			level -=1;
			log.debug(getLevelFormat() + "<Dictionary End> at " + position);
			position += 1;
			
			return position;
		}

		return position;
	}

	protected int parseIntegerOrString(int position, BString b, BInteger i)
			throws BReadException {

		if (i.isInteger(data, position)) {
			Integer int_value = i.read(data, position);
			log.debug(getLevelFormat() + "Integer : " + int_value);

			if (inKeyLength) {
				torrent.addFilesLengthItem(filesIndex, int_value);
			} else if (inKeyPieceLength) {
				torrent.setPieceslength(int_value);
			}
			
			position = i.getActualPosition();
		} else {
			if (inKeyPieces) {
				torrent.setPieces(b.readByte(data, position));
			} else {
				String text_value = b.read(data, position);
				log.debug(getLevelFormat() + "Value : " + text_value);

				// Check internal state
				if (inKeyAnnounce) {
					torrent.setAnnounce(text_value);
				} else if (inKeyAnnounceList) {
					torrent.addNewAnnouceListItem(text_value);
				} else if (inKeyName) {
					if (multipleFiles) {
						torrent.setRootName(text_value);
					} else {
						torrent.addFilesNameItem(filesIndex, text_value);
					}
				} else if (inKeyPath) {
					torrent.addFilesNameItem(filesIndex, text_value);
					filesIndex++;
				}
			}
			position = b.getActualPosition();
		}
		return position;
	}

	public void computeSHA1FromPieces() throws UnsupportedEncodingException {
		if (torrent != null) {
			torrent.computePieces();
		}
	}

	public void extractSHA1FromHashInfo() throws NoSuchAlgorithmException,
			IOException {
		if (torrent != null) {
			byte temp[] = Arrays.copyOfRange(data, info_hash_pos,
					data.length - 1);

			byte info_hash[] = Digest
					.generateSHA1FromHash(temp, 0, temp.length);

			log.debug("SHA1 info_hash is:"
					+ new String(Hex.encodeHex(info_hash, false)));
			torrent.setSHA1infoHash(info_hash);
		}

	}
	
	public Torrent getTorrent() {
		return torrent;
	}

}
