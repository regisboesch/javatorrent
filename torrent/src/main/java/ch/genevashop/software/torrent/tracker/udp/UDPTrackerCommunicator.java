package ch.genevashop.software.torrent.tracker.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Random;

import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.Properties;
import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.peer.Peer;
import ch.genevashop.software.torrent.tracker.TrackerCommunicator;

public class UDPTrackerCommunicator extends TrackerCommunicator {

	static final Logger log = Logger.getLogger(UDPTrackerCommunicator.class);
	private static final int ACTION_CONNECT = 0;
	private static final int ACTION_ANNOUNCE = 1;
	private static final long CONNECTIONID_DEFAULT = 0x41727101980L;
	private static final int SIZECONNECT = 16;
	private static final int SIZEANNOUNCE = 98;
	private static final int SIZEANNOUNCERES = 512;

	private long connectionid;

	public UDPTrackerCommunicator(Torrent torrent, Properties properties) {
		super(torrent, properties);
	}

	@Override
	public boolean connect(URI uri, int left) {
		this.setLeft(left);
		try {
			return connectUDP(uri);
		} catch (IOException e) {
			log.warn("Unnable to connect to " + uri, e);
			return false;
		}
	}

	private boolean connectUDP(URI uri) throws IOException {
		boolean result = false;

		int transactionid = randInt(0, Integer.MAX_VALUE / 2);

		ByteBuffer connect = createConnectAction(transactionid);
		DatagramSocket clientSocket = new DatagramSocket();
		int SOCKETTIMEOUT = 5*1000;
		clientSocket.setSoTimeout(SOCKETTIMEOUT);
		
		// Send connect action
		sendUDPPacket(uri, connect, clientSocket);

		// Receive connect action result
		byte[] receiveData = new byte[SIZECONNECT];
		DatagramPacket receivePacket = new DatagramPacket(receiveData,
				receiveData.length);
		clientSocket.receive(receivePacket);

		if (receiveData.length >= SIZECONNECT) {
			result = validateConnect(ByteBuffer.wrap(receiveData),
					transactionid);
		} else {
			log.debug("Invalid packet size after connection reply");
		}

		// Send announce request
		transactionid = randInt(0, Integer.MAX_VALUE / 2);
		ByteBuffer announce = createAnnounceAction(transactionid);
		sendUDPPacket(uri, announce, clientSocket);

		// Receive announce result
		receiveData = new byte[SIZEANNOUNCERES];
		receivePacket = new DatagramPacket(receiveData, receiveData.length);
		clientSocket.receive(receivePacket);
		processAnnounceResult(ByteBuffer.wrap(receiveData), transactionid);

		clientSocket.close();
		return result;
	}

	private void sendUDPPacket(URI uri, ByteBuffer connect,
			DatagramSocket clientSocket) throws UnknownHostException,
			IOException {
		DatagramPacket sendPacket = new DatagramPacket(connect.array(),
				connect.array().length, InetAddress.getByName(uri.getHost()),
				uri.getPort());
		clientSocket.send(sendPacket);
	}

	private ByteBuffer createConnectAction(int transactionid) {
		ByteBuffer bb = ByteBuffer.allocate(SIZECONNECT);
		bb.clear();
		bb.putLong(CONNECTIONID_DEFAULT);
		bb.putInt(ACTION_CONNECT);
		bb.putInt(transactionid);
		return bb;
	}

	private ByteBuffer createAnnounceAction(int transactionid) {
		ByteBuffer bb = ByteBuffer.allocate(SIZEANNOUNCE);
		bb.clear();
		bb.putLong(getConnectionid());
		bb.putInt(ACTION_ANNOUNCE);
		bb.putInt(transactionid);
		bb.put(getTorrent().getSHA1infoHash());
		bb.put(getProperties().getPeerIdInByte());
		bb.putLong(getDownloaded());
		bb.putLong(getLeft());
		bb.putLong(getUploaded());
		bb.putInt(0); // Default IP address
		bb.putInt(0); // Default Key ?
		bb.putInt(-1);
		bb.putShort(getProperties().getPort());
		return bb;
	}

	private void processAnnounceResult(ByteBuffer bb, int transactionid) {
		int action = bb.getInt();
		if (action == ACTION_ANNOUNCE) {
			int temp_transactionid = bb.getInt();
			if (temp_transactionid == transactionid) {
				this.setIntervalInSecond(bb.getInt());
				this.setIncomplete(bb.getInt());
				this.setComplete(bb.getInt());
				log.debug("Interval : " + this.getIntervalInSecond());
				log.debug("Incomplete : " + this.getIncomplete()
						+ " / Complete : " + this.getComplete());
				
				// Get Peers List
				do {
					byte[] ipBytes = new byte[4];
					bb.get(ipBytes);
					
					if (ipBytes[3] == 0) {
						break;
					}
					
					InetAddress ip;
					try {
						ip = InetAddress.getByAddress(ipBytes);
																		
						int port = (0xFF & (int) bb.get()) << 8
								| (0xFF & (int) bb.get());

						InetSocketAddress ia = new InetSocketAddress(ip, port);
						Peer newPeer = new Peer(ia);
						getPeerslist().put(newPeer.toString(), newPeer);

						log.debug("Add peer :" + ia.toString());
					} catch (UnknownHostException e) {
						log.error("Error on address", e);
						break;
					}
					
				} while(bb.hasRemaining()) ;
			} else {
				log.debug("Wrong transaction id");
				return;
			}
		}
	}

	private boolean validateConnect(ByteBuffer bb, int transactionid) {

		int action = bb.getInt();
		if (action == ACTION_CONNECT) {

			int temp_transactionid = bb.getInt();

			if (temp_transactionid == transactionid) {

				// Store connection id
				this.setConnectionid(bb.getLong());
				log.debug("Connection ID : " + this.getConnectionid());

			} else {
				log.debug("Wrong transaction id");
				return false;
			}
		} else {
			log.debug("Wrong action");
			return false;
		}

		return true;
	}

	public long getConnectionid() {
		return connectionid;
	}

	public void setConnectionid(long connectionid) {
		this.connectionid = connectionid;
	}

	private int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}
}
