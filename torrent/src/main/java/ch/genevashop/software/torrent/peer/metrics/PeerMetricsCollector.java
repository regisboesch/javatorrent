package ch.genevashop.software.torrent.peer.metrics;

import java.util.Map;

import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.peer.Peer;
import ch.genevashop.software.torrent.peer.PeerState;
import ch.genevashop.software.torrent.tracker.TrackerCommunicator;

public class PeerMetricsCollector implements Runnable {

	static final Logger log = Logger.getLogger(PeerMetricsCollector.class);
	private final TrackerCommunicator trackerCommunicator;

	public PeerMetricsCollector(TrackerCommunicator trackerCommunicator) {
		super();
		this.trackerCommunicator = trackerCommunicator;
	}

	@Override
	public void run() {
		
		DataMetrics finalDataMetrics = new DataMetrics();
				
		for (Map.Entry<String, Peer> entry : trackerCommunicator.getPeerslist()
				.entrySet()) {
			
			Peer peer = (Peer) entry.getValue();

			if (peer.getInternalState() == PeerState.HANDSHAKE_RECEIVED) {				
				// Process dataMetrics
				finalDataMetrics.add(peer.getMetrics());			
			}
		}

		log.debug("Global metrics :" + finalDataMetrics.toString());
	}

	public TrackerCommunicator getTrackerCommunicator() {
		return trackerCommunicator;
	}

}
