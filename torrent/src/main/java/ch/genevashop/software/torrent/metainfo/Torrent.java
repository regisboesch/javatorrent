package ch.genevashop.software.torrent.metainfo;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

public class Torrent {

	private static final String TORRENTENCODING = "ISO-8859-1";
	public static final int SIZE_HASH_PIECE = 20;
	private String announce;
	private ArrayList<String> announceList;
	private byte[] SHA1infoHash; 				// info_hash of the torrent
	private ArrayList<byte[]> SHA1piecesList; 	// each SHA1 pieces of the torrent 
	private final ArrayList<TorrentFiles> torrentFiles = new ArrayList<>(); // torrent files in order
	private int pieceslength;
	private byte[] pieces;
	static final Logger log = Logger.getLogger(Torrent.class);
	private String rootName;

	public Torrent() {
		super();
	}

	public void parseTorrent(byte data[]) {

		if (data == null) {
			log.error("Buffer null !!");
			return;
		}

		TorrentParser p = new TorrentParser(data, this);
		p.parse(0);

		try {
			p.extractSHA1FromHashInfo();
		} catch (NoSuchAlgorithmException | IOException e) {
			log.error("Something wrong in parsing :", e);
		}
		try {
			p.computeSHA1FromPieces();
		} catch (UnsupportedEncodingException e) {
			log.error("Something wrong in parsing :", e);
		}
	}

	public String getAnnounce() {
		return announce;
	}

	public void setAnnounce(String announce) {
		this.announce = announce;
	}

	public ArrayList<String> getAnnounceList() {
		return announceList;
	}

	public void addNewAnnouceListItem(String item) {
		if (announceList == null) {
			announceList = new ArrayList<>();
		}
		announceList.add(item);
	}

	public void computePieces() throws UnsupportedEncodingException {

		int numberofpieces = pieces.length / SIZE_HASH_PIECE;
		log.debug("Number of pieces :" + numberofpieces);
		SHA1piecesList = new ArrayList<>();

		int piece = 0;
		while (piece < numberofpieces) {

			byte[] t = Arrays.copyOfRange(pieces, SIZE_HASH_PIECE * piece,
					SIZE_HASH_PIECE * (piece + 1));

			SHA1piecesList.add(t);
			piece++;
		}
	}

	public ArrayList<TorrentFiles> getTorrentFiles() {
		return torrentFiles;
	}

	public int getPieceslength() {
		return pieceslength;
	}

	public void setPieceslength(int pieceslength) {
		this.pieceslength = pieceslength;
	}

	public byte[] getPieces() {
		return pieces;
	}

	public void setPieces(byte[] pieces) {
		this.pieces = pieces;
	}

	public ArrayList<byte[]> getSHA1piecesList() {
		return SHA1piecesList;
	}

	public int getNumberOfPieces() {
		return SHA1piecesList !=null ? SHA1piecesList.size():0;
	}
	
	public byte[] getSHA1infoHash() {
		return SHA1infoHash;
	}

	public String getSHA1infoHashForGet() {

		try {
			return URLEncoder.encode(new String(SHA1infoHash, TORRENTENCODING),
					TORRENTENCODING);
		} catch (UnsupportedEncodingException e) {
			log.error("Unable to encode SHA1 byte", e);
			return null;
		}
	}

	public String getSHA1infoHashForFile() {
		return Hex.encodeHexString(SHA1infoHash);
	}
	
	public void setSHA1infoHash(byte[] sHA1infoHash) {
		SHA1infoHash = sHA1infoHash;
	}

	public void addFilesNameItem(int index, String name) {
		this.checkTorrentFIlesItemSize(index);
		this.getTorrentFiles().get(index).setName(name);
	}

	public void addFilesLengthItem(int index, int length) {
		this.checkTorrentFIlesItemSize(index);
		this.getTorrentFiles().get(index).setLength(length);
	}
	
	private void checkTorrentFIlesItemSize(int index) {
		if (this.getTorrentFiles().size() <= index) {
			this.getTorrentFiles().add(index, new TorrentFiles());
		}
	}

	public int computeTotalLength() {
		int total = 0;
		for (TorrentFiles tf : this.getTorrentFiles()) {
			total += tf.getLength();
		}
		return total;
	}

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

}
