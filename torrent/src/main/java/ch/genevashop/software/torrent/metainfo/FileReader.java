package ch.genevashop.software.torrent.metainfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

public class FileReader {

	static final Logger log = Logger.getLogger(FileReader.class);

	public byte[] readFromRessource(String absoluteRessource) {
		InputStream is = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(absoluteRessource);
		return readInputStream(is);
	}

	public byte[] readFromFile(String absoluteFile)
			throws FileNotFoundException {
		InputStream is = new FileInputStream(new File(absoluteFile));
		return readInputStream(is);
	}

	private byte[] readInputStream(InputStream fis) {

		int nRead;
		byte[] data = new byte[2048];
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		try {
			while ((nRead = fis.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			
			buffer.flush();
		} catch (IOException e1) {
			log.error("unable to read torrent file", e1);
		}

		finally {		
			try {
				fis.close();
			} catch (IOException e) {
				log.error("unable to close inputstream form torrent file :", e);
			}
		}

		return buffer.toByteArray();
	}
}
