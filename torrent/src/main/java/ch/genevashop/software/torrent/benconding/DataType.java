package ch.genevashop.software.torrent.benconding;

public interface DataType<U> {
	
	public final static char BDELIMITER = ':';	
	public U read(byte dataIn[], int position) throws BReadException;
	public byte[] write(U dataout) throws BWriteException;
	public int getActualPosition();
}
