package ch.genevashop.software.torrent.tracker;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.utils.URIBuilder;
import org.apache.log4j.Logger;

import ch.genevashop.software.torrent.Properties;
import ch.genevashop.software.torrent.file.TorrentFile;
import ch.genevashop.software.torrent.file.TorrentFilesStorage;
import ch.genevashop.software.torrent.file.TorrentStorage;
import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.peer.Peer;
import ch.genevashop.software.torrent.peer.PeerCommunicator;
import ch.genevashop.software.torrent.peer.metrics.PeerMetricsCollector;
import ch.genevashop.software.torrent.tracker.tcp.TCPTrackerCommunicator;
import ch.genevashop.software.torrent.tracker.udp.UDPTrackerCommunicator;

public class TorrentTracker {

	private Properties properties;
	private Torrent torrent;	
	private final PeerCommunicator peerCommunication;
	private TrackerCommunicator t = null;
	private TorrentFile torrentFile;
	private TorrentStorage torrentStorage;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private TorrentDecision torrentDecision;
	
	static final Logger log = Logger.getLogger(TorrentTracker.class);

	public TorrentTracker(Properties properties, Torrent torrent) {
		super();
		this.properties = properties;
		this.torrent = torrent;					
		this.peerCommunication = new PeerCommunicator(this);
	}

	public void startSession() throws URISyntaxException,
			ClientProtocolException, IOException {
		
		// Open or Create torrent file for temporary peer in dex state
		torrentFile = new TorrentFile(torrent.getSHA1infoHashForFile(), torrent.getNumberOfPieces());		
		torrentFile.readState();
		torrentFile.updateState();
		
		// CreateFiles of the torrent
		torrentStorage = new TorrentFilesStorage();
		torrentStorage.initializeTorrentFiles(this.getTorrent());
		
		// Calculate left byte to download
		int left = torrent.computeTotalLength();
		
		// Choose http announce or announce-list
		String adress = torrent.getAnnounce();
		URI uri = new URIBuilder(adress).build();					

		int index = 0;
		boolean connected = false;
		do {
			adress = torrent.getAnnounceList().get(index);
			log.debug("Try for announce :" + adress);
			uri = new URIBuilder(adress).build();

			if (uri.getScheme().endsWith("udp")) {
				t = new UDPTrackerCommunicator(torrent, properties);				
			} else {
				t = new TCPTrackerCommunicator(torrent, properties);
			}
			connected = t.connect(uri, left);

		} while (!connected && ((++index) < torrent.getAnnounceList().size()));		
		
		// Launch MetricsCollector Runnable Task in peerCommunicator
		launchMetrics(t);			
		
		if (t!=null) {
			// Connect Peer list
			peerCommunication.connectPeerList(t.getPeerslist());
		} else {
			log.info("No tracker responding");
		}

		peerCommunication.launchPiecePolicyAlgorithm();
		log.debug("Peer Algorithm Policy launched");
		
		launchTorrentDecision();
		log.debug("Torrent decision launched");
	}			
	
	private void launchTorrentDecision() {
		torrentDecision = new TorrentDecision(this);
		
		// TODO : find better solution
		try {
			Thread.sleep(60 * 1000);
		} catch (InterruptedException e) {
			log.error(e);
		}
		
		torrentDecision.start();
	}

	private void launchMetrics(TrackerCommunicator t) {
		PeerMetricsCollector peerMetricsCollector = new PeerMetricsCollector(t);
		scheduler.scheduleAtFixedRate(peerMetricsCollector, 10, 10, java.util.concurrent.TimeUnit.SECONDS);		
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public Torrent getTorrent() {
		return torrent;
	}

	public void setTorrent(Torrent torrent) {
		this.torrent = torrent;
	}	

	public PeerCommunicator getPeerCommunication() {
		return peerCommunication;
	}

	public Map<String, Peer> getPeerslist() {
		return t!=null?t.getPeerslist():null;
	}

	public Peer getPeer(String peer) {
		return t.getPeerslist().get(peer);
	}
	
	public TorrentFile getTorrentFile() {
		return torrentFile;
	}

	public void setTorrentFile(TorrentFile torrentFile) {
		this.torrentFile = torrentFile;
	}
	
	public byte[] getSHA1HashFromPiece(int index) {
		return t.getTorrent().getSHA1piecesList().get(index);
	}

	public TorrentStorage getTorrentStorage() {
		return torrentStorage;
	}
		
}
