package ch.genevashop.software.torrent.peer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import ch.genevashop.software.tools.ByteTools;
import ch.genevashop.software.torrent.peer.protocol.Handshake;
import ch.genevashop.software.torrent.peer.protocol.PeerMessage;

public class PeerSelector implements Runnable {

	private static final int READTIMEOUT = 200;
	static final Logger log = Logger.getLogger(PeerSelector.class);
	static final int BUFFERSIZE = 16 * 1024;
	static final int BUFFERSIZEPEERMESSAGES = 4;
	private PeerCommunicator peerCommunicator;
	private Selector selector;
	private boolean isRunning;
	private Set<Integer> pieces;
	private ConcurrentLinkedQueue<ByteBuffer> messageTosend = new ConcurrentLinkedQueue<>();
	private long lastTimeAlive;

	{
		isRunning = false;
		try {
			selector = Selector.open();
		} catch (IOException e) {
			selector = null;
		}

		pieces = new HashSet<>();
		lastTimeAlive = System.currentTimeMillis();
	}

	public PeerSelector(PeerCommunicator peerCommunicator) {
		this.peerCommunicator = peerCommunicator;
	}

	@Override
	public void run() {
		isRunning = true;
		startSelector();
	}

	private void startSelector() {

		while (isRunning) {

			int readyChannels;
			try {
				readyChannels = selector.select();
			} catch (IOException e) {
				log.error("Unable to select", e);
				continue;
			}

			if (readyChannels == 0)
				continue;

			Set<SelectionKey> selectedKeys = selector.selectedKeys();

			Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

			while (keyIterator.hasNext()) {

				SelectionKey key = keyIterator.next();
				String peerKey = (String) key.attachment();
				SocketChannel sc = (SocketChannel) key.channel();

				if (key.isAcceptable()) {
					// a connection was accepted by a ServerSocketChannel.

				} else if (key.isConnectable()) {

					if (peerCommunicator.getTracker().getPeerslist()
									.get(peerKey).getInternalState() == PeerState.NOTCONNECTED) {
						// a connection was established with a remote server.
						try {
	
							if (sc.finishConnect()) {
	
								peerCommunicator.getTracker().getPeerslist()
										.get(peerKey)
										.setInternalState(PeerState.CONNECTED);
								
								log.debug("Peer [" + peerKey.toString() + "] connected");
								
							} else {
								key.cancel();
								isRunning = false;
								try {
									sc.close();
								} catch (IOException e) {
									log.error(e);
								}
							}
	
						} catch (IOException e) {
							log.error("Unable to connect to" + peerKey);
							key.cancel();
							isRunning = false;
							try {
								sc.close();
							} catch (IOException e2) {
								log.error(e2);
							}
							continue;
						}
					}	

				} else if (key.isReadable()) {

					// a channel is ready for reading
					readBuffer(sc, peerKey);

				} else if (key.isWritable()) {
					// a channel is ready for writing

					// Check if connected -> Send handshake
					if (peerCommunicator.getTracker().getPeerslist()
							.get(peerKey).getInternalState() == PeerState.CONNECTED) {

						// Send Handshake
						sendHandshake(sc, peerKey);
					} else if (!messageTosend.isEmpty()) {
						// Check if we need to send a message
						sendMessage(sc, peerKey);
					}

				}
				
				keyIterator.remove();
								
				try {
					Thread.sleep(READTIMEOUT);
				} catch (InterruptedException e) {
					log.error(e);
				}
				
			}
		}
	}

	private void readBuffer(SocketChannel sc, String peerKey) {
		if (peerCommunicator.getTracker().getPeerslist().get(peerKey)
				.getInternalState() == PeerState.HANDSHAKE_SEND) {
			waitHandshakeReply(sc, peerKey);
		} else if (peerCommunicator.getTracker().getPeerslist().get(peerKey)
				.getInternalState() == PeerState.HANDSHAKE_RECEIVED) {
			processClientMessage(sc, peerKey);
		}
	}

	private void processClientMessage(SocketChannel sc, String peerKey) {
		PeerMessage p = new PeerMessage();

		ByteBuffer bb = ByteBuffer
				.allocate(PeerSelector.BUFFERSIZEPEERMESSAGES);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.rewind();

		// Get the length of the message
		while (true) {
			try {
				sc.read(bb);

				if (bb.hasRemaining()) {
					try {
						Thread.sleep(READTIMEOUT);
					} catch (InterruptedException e) {
						log.error(e);
					}
				} else {
					break;
				}

			} catch (IOException e) {
				log.error("Error on readBuffer", e);
				closeConnection(sc, peerKey);
				return;
			}
		}

		// Get the data
		int pstrlen = bb.getInt(0);

		if (pstrlen > 0) {
			bb = ByteBuffer.allocate(pstrlen);
			bb.order(ByteOrder.BIG_ENDIAN);
			bb.rewind();

			while (true) {

				try {
					sc.read(bb);
				} catch (IOException e) {
					log.error("Error on readBuffer", e);
					closeConnection(sc, peerKey);
					return;
				}

				if (bb.hasRemaining()) {
					try {
						Thread.sleep(READTIMEOUT);
					} catch (InterruptedException e) {
						log.error(e);
					}
				} else {
					break;
				}
			}

			bb.rewind();
			parseReadMessage(peerKey, p, bb);
		}
	}

	private void waitHandshakeReply(SocketChannel sc, String peerKey) {
		ByteBuffer bb = ByteBuffer.allocate(Handshake.SIZEHANDSHAKE);
		int numbytesRead = 0;

		do {
			try {
				numbytesRead += sc.read(bb);
			} catch (IOException e) {
				log.error("Error on readBuffer", e);

				closeConnection(sc, peerKey);
			}

			if (numbytesRead <= 0) {
				return;
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				log.error(e);
			}

		} while (bb.hasRemaining());

		bb.flip();

		byte peerid[] = Handshake.validateHandshakeAndGeetPeerID(bb);

		if (peerid != null) {
			peerCommunicator.getTracker().getPeerslist().get(peerKey)
					.setPeer_id(peerid);

			// Check String
			peerCommunicator.getTracker().getPeerslist().get(peerKey)
					.setInternalState(PeerState.HANDSHAKE_RECEIVED);

			// Process bytesRead
			log.debug("Process Read Handshake [" + numbytesRead + "] of peer "
					+ peerKey + " / PeerID : " + Hex.encodeHexString(peerid));

		} else {

			closeConnection(sc, peerKey);
		}
	}

	private void closeConnection(SocketChannel sc, String peerKey) {
		// Remove from selector
		sc.keyFor(selector).cancel();
		try {
			sc.close();
		} catch (IOException e) {
			log.error(e);
		}

		// Remove from tracker peer list
		peerCommunicator.getTracker().getPeerslist().remove(peerKey);

		// No more running
		isRunning = false;
	}

	private void parseReadMessage(String peerKey, PeerMessage p, ByteBuffer bb) {

		// Parse Message
		p = PeerMessage.parse(bb);
		if (p != null) {

			log.debug("Message [" + p.getPeerMessageEnum().name()
					+ "] from peer" + peerKey);

			switch (p.getPeerMessageEnum()) {
			case HAVE:
				processHaveMessage(peerKey, p);
				break;
			case BITFIELD:
				processBitfieldMessage(peerKey, p);
				break;
			case UNCHOKE:
				processUnchokeMessage(peerKey);
				break;
			case CHOKE:
				processChokeMessage(peerKey);
				break;
			case PIECE:
				processPieceMessage(peerKey, p);
			default:
				break;
			}
		} else {
			log.debug("message null");
		}
	}

	private void processPieceMessage(String peerKey, PeerMessage p) {
		ByteBuffer bb = ByteBuffer.wrap(p.getData());
		int index = bb.getInt();
		int offset = bb.getInt();
		byte data[] = new byte[p.getData().length - 8];
		bb.get(data);
		peerCommunicator.getTracker().getPeerslist().get(peerKey)
				.updateDownload(data.length);
		peerCommunicator.writeToPiece(data, index, offset);	
		
		// Decrease peer chunk by one
		peerCommunicator.getTracker().getPeer(peerKey).decreaseChunkByOne();
	}

	private void processChokeMessage(String peerKey) {
		peerCommunicator.getTracker().getPeer(peerKey).setPeerchoked(true);
	}

	private void processUnchokeMessage(String peerKey) {
		peerCommunicator.getTracker().getPeer(peerKey).setPeerchoked(false);
	}

	private void processBitfieldMessage(String peerKey, PeerMessage p) {
		for (int i = 0; i < this.getPeerCommunicator().getTracker()
				.getTorrent().getNumberOfPieces(); i++) {
			if (ByteTools.getBit(p.getData(), i) == 1) {
				addPiecesToPeerSet(peerKey, i);
			}
		}
	}

	private void processHaveMessage(String peerKey, PeerMessage p) {
		ByteBuffer wrapped = ByteBuffer.wrap(p.getData());
		int piecesNumber = wrapped.getInt();
		addPiecesToPeerSet(peerKey, piecesNumber);
	}

	private void addPiecesToPeerSet(String peerKey, int piecesNumber) {
		this.getPieces().add(piecesNumber);
		this.getPeerCommunicator().incrementPieceFromPeers(piecesNumber);
		log.debug("Add pieces [" + piecesNumber + "] to peer" + peerKey);
	}

	private void sendHandshake(SocketChannel sc, String peerKey) {

		ByteBuffer bb = Handshake.createHandshake(peerCommunicator.getTracker()
				.getTorrent().getSHA1infoHash(), peerCommunicator.getTracker()
				.getProperties().getPeer_id());

		peerCommunicator.getTracker().getPeerslist().get(peerKey)
				.setInternalState(PeerState.HANDSHAKE_SEND);

		bb.flip();

		try {
			sc.write(bb);
		} catch (IOException e) {
			log.error("Error on writing buffer", e);
		}

		log.debug("Write [" + Hex.encodeHexString(bb.array()) + "] to peer "
				+ peerKey);
	}

	public void addMessageToSend(ByteBuffer bb) {
		messageTosend.offer(bb);
	}

	public void sendMessage(SocketChannel sc, String peerKey) {
		ByteBuffer bb = messageTosend.poll();
		if (bb != null) {
			try {
				sc.write(bb);
			} catch (IOException e) {
				log.error("Error on writing buffer", e);
			}
		}
	}

	public boolean hasPiece(Integer piece) {
		return pieces.contains(piece);
	}

	public Selector getSelector() {
		return selector;
	}

	public Set<Integer> getPieces() {
		return pieces;
	}

	public PeerCommunicator getPeerCommunicator() {
		return peerCommunicator;
	}

}
