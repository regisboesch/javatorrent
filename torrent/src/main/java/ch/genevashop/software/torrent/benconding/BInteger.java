package ch.genevashop.software.torrent.benconding;

import java.util.Arrays;

public class BInteger extends DataTypePosition implements DataType<Integer> {

	private final static char CHARINTEGERSTART = 'i';
	private final static char CHARINTEGEREND = 'e';
	
	public Integer read(byte dataIn[], int position) throws BReadException {

		internalPosition = position;
		
		if ((char)dataIn[position] != CHARINTEGERSTART) {
			throw new BReadException("Not an BInteger");
		}
		
		int positionStart = position+1;	
		
		while (((char)dataIn[position] != CHARINTEGEREND) && (position<dataIn.length)) {
			position++;
		}
		
		int positionEnd = position;		
								
		internalPosition = positionEnd+1;
		
		return new Integer(new String(Arrays.copyOfRange(dataIn, positionStart, positionEnd)));
	}

	public byte[] write(Integer dataout) throws BWriteException {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isInteger(byte dataIn[], int position) {
		
		if ((char)dataIn[position] != CHARINTEGERSTART) {
			return false;
		}			
		
		return true;		
	}
}
