package ch.genevashop.software.torrent.peer;

import java.util.Map;

import ch.genevashop.software.torrent.metainfo.Torrent;

public interface PeerCommunicatorData {

	public abstract Torrent getTorrent();

	public abstract Map<Integer, Chunk> getChunkSet();

	public abstract void allocateNewPiece(int piece);

	public abstract byte[] getChunkData();

	public abstract boolean isPieceComplete();

	public abstract void writeToPiece(byte[] data, int offset);

	public abstract void resetCurrentdata();
	
	public abstract int getCurrentPieceIndex();

}