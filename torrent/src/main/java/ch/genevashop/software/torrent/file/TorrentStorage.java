package ch.genevashop.software.torrent.file;

import ch.genevashop.software.torrent.metainfo.Torrent;

public interface TorrentStorage {

	public void initializeTorrentFiles(Torrent torrent);

	public void writeRelativePosition(byte data[], int position);

	public byte[] readRelativePosition(int position, int size);

}