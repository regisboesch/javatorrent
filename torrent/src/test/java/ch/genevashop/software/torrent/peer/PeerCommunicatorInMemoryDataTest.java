package ch.genevashop.software.torrent.peer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import org.junit.Test;

import ch.genevashop.software.torrent.metainfo.Torrent;

public class PeerCommunicatorInMemoryDataTest {

	private static final int TOTALSIZE = 635265;
	private static final int PIECESLENGTH = 262144;

	@Test
	public void testTorrent() throws UnsupportedEncodingException {
		
		// Construct torrent		
		Torrent torrent = new Torrent();		
		torrent.setPieceslength(PIECESLENGTH);		
		torrent.addFilesNameItem(0, "Essai1.txt");
		torrent.addFilesLengthItem(0, 300000);
		torrent.addFilesNameItem(1, "Essai2.txt");
		torrent.addFilesLengthItem(1, 200000);
		torrent.addFilesNameItem(2, "Essai3.txt");
		torrent.addFilesLengthItem(2, 135265);			
		assertEquals(TOTALSIZE, torrent.computeTotalLength());
		
		// SHA1 of pieces (3 pieces)
		byte SHA1Byte[] = new byte[3*Torrent.SIZE_HASH_PIECE];
		Arrays.fill(SHA1Byte, (byte)0);
		torrent.setPieces(SHA1Byte);
		torrent.computePieces();		
		assertEquals(3, torrent.getNumberOfPieces());
		
		// Construct PeerCommunicator
		PeerCommunicatorInMemoryData p = new PeerCommunicatorInMemoryData(torrent);
		
		// Test piece 0
		p.allocateNewPiece(0);
		assertEquals(16, p.getChunkSet().size());
		int totalSize = 0;
		for (Chunk c : p.getChunkSet().values()) {
			if (c!=null) {
				totalSize += c.getLength();
			}
		}
		// Test a chunk
		assertEquals(PeerCommunicatorInMemoryData.CHUNKSIZE, p.getChunkSet().get(5).getLength());
		// Test last chunk
		assertEquals(PeerCommunicatorInMemoryData.CHUNKSIZE, p.getChunkSet().get(15).getLength());
		// Test total Size
		assertEquals(262144, totalSize);
		
		p.resetCurrentdata();
		assertTrue(p.getChunkSet().isEmpty());
		
		// Test piece 2
		p.allocateNewPiece(2);
		assertEquals(7, p.getChunkSet().size());				
		totalSize = 0;
		for (Chunk c : p.getChunkSet().values()) {
			if (c!=null) {
				totalSize += c.getLength();
			}
		}
		// Test a chunk
		assertEquals(PeerCommunicatorInMemoryData.CHUNKSIZE, p.getChunkSet().get(5).getLength());
		// Test last Chunk
		assertEquals(12673, p.getChunkSet().get(6).getLength());
		// Test total Size
		assertEquals(110977, totalSize);
	}
}
