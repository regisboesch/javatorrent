package ch.genevashop.software.torrent.benconding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.junit.Test;

import ch.genevashop.software.torrent.metainfo.FileReader;
import ch.genevashop.software.torrent.metainfo.Torrent;

public class MetainfoTest {

	private static final int PIECES = 2640;
	private static final int PIECELENGTH = 2097152;
	private static final int LENGTH = 275676396;
	private static final String NAME = "Black Mirror - S01E01 - FRENCH SDTV X264-Spikie.mp4";
	private static final String ANNOUNCE = "udp://exodus.desync.com:6969";
	private static final String SHA1INFO = "5e22ce234e1cadf988177588384e37d8ff85e2e6";
	static final Logger log = Logger.getLogger(MetainfoTest.class);

	@Test
	public void testMetaInfoOneFile() throws UnsupportedEncodingException {

		// Read resource
		FileReader fr = new FileReader();
		byte data[] = fr.readFromRessource("file2.torrent");
		log.debug("New Parser -------- File length :" + data.length);
		assertTrue(data.length > 0);

		// Parse resource
		Torrent t = new Torrent();
		t.parseTorrent(data);

		// Assert SHA1 info key
		assertTrue(SHA1INFO.equals(new String(Hex.encodeHex(
				t.getSHA1infoHash(), true))));

		// Assert key/values
		assertTrue(ANNOUNCE.equals(t.getAnnounce()));
		assertTrue(t.getAnnounceList().size() > 0);
		assertTrue(NAME.equals(t.getTorrentFiles().get(0).getName()));
		assertEquals(LENGTH, t.getTorrentFiles().get(0).getLength());
		assertEquals(PIECELENGTH, t.getPieceslength());
		assertEquals(PIECES, t.getPieces().length);

		// Compute SHA1 pieces
		assertTrue(t.getSHA1piecesList().size() == PIECES / 20);
		assertEquals(
				"49FB34810E81630191DE18B91DCB15B9DA64D66D",
				new String(Hex.encodeHex(
						t.getSHA1piecesList().get(
								t.getSHA1piecesList().size() - 1), false)));
	}

	@Test
	public void testMetaInfoMultipleFiles() {
		// Read resource
		FileReader fr = new FileReader();
		byte data[] = fr.readFromRessource("file3.torrent");
		log.debug("New Parser -------- File length :" + data.length);
		assertTrue(data.length > 0);

		// Parse resource
		Torrent t = new Torrent();
		t.parseTorrent(data);

		// Assert SHA1 info key
		assertTrue("0F83F4542DDD7F83F439CCBFA94C01A78BD0DEC7".equals(new String(Hex.encodeHex(
				t.getSHA1infoHash(), false))));
		
		assertEquals(27, t.getTorrentFiles().size());
	}
}
