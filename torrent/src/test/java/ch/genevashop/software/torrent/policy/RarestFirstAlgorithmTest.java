package ch.genevashop.software.torrent.policy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import ch.genevashop.software.torrent.file.TorrentFile;

public class RarestFirstAlgorithmTest {

	@Test
	public void test() {
		RarestFirstAlgorithm d = new RarestFirstAlgorithm();		
		int SIZE = 96;		
		TorrentFile tf = new TorrentFile("Essai", SIZE);
		
		// 1 - Random piece if no downloaded piece
		Map<Integer, Integer> pieces = new HashMap<>();
		for (int index=0; index < SIZE;index++) {
			pieces.put(index, SIZE-index);
		}
		
		int piece_1 = d.getNextPiece(pieces, tf);
		int piece_2 = d.getNextPiece(pieces, tf);		
		assertEquals(piece_1, piece_2);
		
		d.setPieceComplete(piece_1);
		piece_2 = d.getNextPiece(pieces, tf);
		assertFalse(piece_1==piece_2);
		d.setPieceComplete(piece_2);
		
		// 2 - Rarest
		for (int index=0; index < 10; index++) {
			tf.setBitAndUpdate(index, 1);
		}
		
		piece_1 = d.getNextPiece(pieces, tf);
		assertEquals(95, piece_1);
		piece_2 = d.getNextPiece(pieces, tf);
		assertEquals(piece_1, piece_2);
	}
	
}
