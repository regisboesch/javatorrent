package ch.genevashop.software.torrent.file;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TorrentFileTest {

	@Test
	public void test1() {
		int SIZE = 328;
		
		TorrentFile tf = new TorrentFile("Essai", SIZE);		
		assertTrue(tf.hasNoPieceDownloaded());
		assertFalse(tf.isAlmostDone());
		assertFalse(tf.isDone());
		for (int index=0; index < 328-8; index++) {
			tf.setBitAndUpdate(index, 1);
		}
		
		assertFalse(tf.hasNoPieceDownloaded());
		assertTrue(tf.isAlmostDone());
		assertFalse(tf.isDone());
		
		for (int index=320; index < 328; index++) {
			tf.setBitAndUpdate(index, 1);
		}
		
		assertFalse(tf.hasNoPieceDownloaded());
		assertTrue(tf.isAlmostDone());
		assertTrue(tf.isDone());
	}
}
