package ch.genevashop.software.torrent.benconding;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class DataTypeTest {

	static final Logger log = Logger.getLogger(DataTypeTest.class);
	
	@Test
	public void testBString() throws BReadException {
		String b1 = "maistestezmoi";
		String b2 = "" + b1.length() + DataType.BDELIMITER + b1;

		log.debug("String to test: "+b2);
		BString b = new BString();
		StringBuffer buf = new StringBuffer(b2);
		String res = b.read(buf.toString().getBytes(), 0);
		assertEquals(b1, res);

	}

	@Test
	public void testInteger() throws BReadException {

		String s1 = "i342e4444";
		Integer i1 = 342;

		BInteger b = new BInteger();
		assertEquals(i1, b.read(s1.getBytes(), 0));
	}
}
