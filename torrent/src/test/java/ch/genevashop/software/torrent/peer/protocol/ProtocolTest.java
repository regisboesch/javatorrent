package ch.genevashop.software.torrent.peer.protocol;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProtocolTest {

	@Test 
	public void test1(){
		byte messageID = 5;
		PeerMessageEnum p =  PeerMessageEnum.getEnumByMessageID(messageID );
		assertEquals(messageID, p.getMessageId());
	}
	
	@Test 
	public void test2(){
		byte messageID = 15;
		byte messageIDOut = (byte) 0xFF;
		PeerMessageEnum p =  PeerMessageEnum.getEnumByMessageID(messageID );
		assertEquals(messageIDOut, p.getMessageId());
	}
}
