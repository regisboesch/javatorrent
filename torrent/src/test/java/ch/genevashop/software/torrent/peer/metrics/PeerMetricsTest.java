package ch.genevashop.software.torrent.peer.metrics;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PeerMetricsTest {

	@Test
	public void test1() throws InterruptedException {
		PeerMetrics p = new PeerMetrics();
		
		p.computeRatio();
		assertEquals(0, p.getLastDowndloadRatio());
		assertEquals(0, p.getLastUploadRatio());
		Thread.sleep(1000);
		
		p.updateDownload(500);
		p.updateUpload(500);
		Thread.sleep(500);
		
		p.updateDownload(500);
		p.updateUpload(500);
		Thread.sleep(500);
		
		p.computeRatio();
		assertEquals(1000, p.getLastDowndloadRatio());
		assertEquals(1000, p.getLastUploadRatio());
		
		Thread.sleep(1000);
		p.computeRatio();
		assertEquals(0, p.getLastDowndloadRatio());
		assertEquals(0, p.getLastUploadRatio());
	}
}
