package ch.genevashop.software.torrent.udp;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import ch.genevashop.software.torrent.Properties;
import ch.genevashop.software.torrent.metainfo.Torrent;
import ch.genevashop.software.torrent.tracker.udp.UDPTrackerCommunicator;

public class UDPTrackerCommunicationTest {

	@Test
	public void testAnnounceParsing() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, DecoderException {
		String bufferIn = "000000014d4903150000065c0000044100004283af8c5b22f4a229a0a9dcd31125304743e414c7f187b2d52f54eaf25eb730b4bf49a3282d7bd3e0957799bad87fc1b38d3c305d39af60c074805a4f5355e20aa5a6e43aad55354ab4be53d5f7848dbc066553f4bf7b3fedd9dab3bc0663c0dfc87a95a32b73fb7b1a097433654e86f07370a30596df66ee7c0e8bd043ed103a06fb612fbd671f5a7a4be2d850203a677db759c0423c1855340ce861a8be5016533a144fa0d297e508a2e4c9e1c3115693c44be6254cb19af01ae1b343cbebd2433aad1db2612a256b0d24b56b2942e7efeed1c9e5d37687e15596d2eac3557db62bba378f9e27bbd236d5538e05319f67c87d6d8dc8d5b0cb0f2865e5528364b2c928601317c181af5f5df8b4d4de258ea3e9e3b879d32b6154cdbb22a94e3b095da340b6d171558108c4d144000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
		int transactionid = 1296630549;
		
		Torrent t = new Torrent();
		Properties p = new Properties();
		UDPTrackerCommunicator u = new UDPTrackerCommunicator(t,p);
		
		@SuppressWarnings("rawtypes")
		Class cArg[] = new Class[] {ByteBuffer.class, int.class};
		
		Method method = u.getClass().getDeclaredMethod("processAnnounceResult", cArg);
		method.setAccessible(true);
		
		Object[] o = new Object[] {ByteBuffer.wrap(Hex.decodeHex(bufferIn.toCharArray())), transactionid};
		method.invoke(u, o);
				
		assertEquals(1628, u.getIntervalInSecond());
		assertEquals(1089, u.getIncomplete());
		assertEquals(17027, u.getComplete());
		assertEquals(50, u.getPeerslist().size());
		assertEquals("/175.140.91.34:62626" , u.getPeerslist().get("/175.140.91.34:62626").toString());
	}
}
