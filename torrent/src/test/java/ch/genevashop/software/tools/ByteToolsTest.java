package ch.genevashop.software.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;

public class ByteToolsTest {

	static final Logger log = Logger.getLogger(ByteToolsTest.class);

	@Test
	public void test1() {
		byte data[] = new byte[] { (byte) 0xFF, (byte) 0xFF };
		byte dataref[] = new byte[] { (byte) 0x7F, (byte) 0x7F };

		ByteTools.setBit(data, 0, 0);
		ByteTools.setBit(data, 8, 0);

		assertArrayEquals(dataref, data);
		assertEquals(0, ByteTools.getBit(data, 0));
		assertEquals(1, ByteTools.getBit(data, 1));
		assertEquals(0, ByteTools.getBit(data, 8));
		assertEquals(1, ByteTools.getBit(data, 9));
	}
}
