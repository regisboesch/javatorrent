[![CC-BY-NC-SA](http://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png "Licence Creative Commons")](http://creativecommons.org/licenses/by-nc-sa/4.0/)
Creative Commons License
Java Torrent by Régis Boesch is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
Based on a work at https://bitbucket.org/regisboesch/javatorrent/.